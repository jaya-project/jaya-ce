﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using NLog;
using System.Data;
using System.IO;

namespace InventoryLibraries.SQLite
{
    public class SQLiteWrapper
    {
        private static SQLiteWrapper Self = null;
        private string ConnStr;
        private SQLiteCommand Cmd;
        private static Logger log = LogManager.GetLogger("SQLiteWrapper");
        private int RowsUpdated = -1;

        public SQLiteWrapper(string dbFile)
        {
            string file = "";

            if (File.Exists(dbFile))
                file = dbFile;
            else
                file = "inventory.db";

            log.Debug("Database file path: {0}", dbFile);
            ConnStr = string.Format("Data Source={0}; Version=3; Pooling=true; Max Pool Size=100", file);
            EnableForeignKey();
        }

        public SQLiteWrapper(Dictionary<string, string> connectionOptions)
        {
            StringBuilder tmp = new StringBuilder();

            foreach (KeyValuePair<string, string> kvp in connectionOptions)
            {
                tmp.AppendFormat("{0}={1}; ", kvp.Key, kvp.Value);
            }

            ConnStr = tmp.ToString().Trim("; ".ToCharArray());
            EnableForeignKey();
        }

        #region GetInstance

        public static SQLiteWrapper GetInstance(string dbFile)
        {
            if (Self == null)
            {
                Self = new SQLiteWrapper(dbFile);
            }

            return Self;
        }

        public static SQLiteWrapper GetInstance(Dictionary<string, string> connectionOptions)
        {
            if (Self == null)
            {
                Self = new SQLiteWrapper(connectionOptions);
            }

            return Self;
        }

        public static SQLiteWrapper GetInstance()
        {
            return Self;
        }

        #endregion GetInstance

        private SQLiteConnection Connect()
        {
            SQLiteConnection link = null;

            try
            {
                link = new SQLiteConnection(ConnStr);
                link.Open();
                /*link.Flags = SQLiteConnectionFlags.LogAll;
                SQLiteLog.Enabled = true;
                SQLiteLog.Log += new SQLiteLogEventHandler(SqliteLogHandler);*/
            }
            catch (SQLiteException e)
            {
                log.Error("Could not connect to SQLite: {0}", e);
            }

            return link;
        }

        private void SqliteLogHandler(object sender, LogEventArgs e)
        {
            log.Debug("SQLite Logging: {0}", e.Message);
        }

        private void Close()
        {
            if (Cmd != null)
            {
                if (Cmd.Connection != null)
                {
                    try
                    {
                        Cmd.Connection.Close();
                        Cmd.Parameters.Clear();
                        Cmd.Dispose();
                        Cmd = null;
                    }
                    catch (SQLiteException e)
                    {
                        log.Error("Error trying to close Database Connection: {0}", e);
                    }
                }
            }
        }

        private void EnableForeignKey()
        {
            log.Debug("Trying to enable foreign keys...");

            try
            {
                Cmd = new SQLiteCommand(Connect());
                Cmd.CommandText = "PRAGMA foreign_keys = ON";
                int updates = Cmd.ExecuteNonQuery();
                log.Debug("Rows modified: {0}", updates);
                Close();
            }
            catch (SQLiteException e)
            {
                log.Error("Failed to enable foreign keys: {0}", e);
            }
        }

        #region Insert
        public int Insert(string table, List<List<object>> values)
        {
            return Insert(table, "", values, "");
        }

        public int Insert(string table, List<string> columns, List<List<object>> values)
        {
            return Insert(table,
                String.Join(",", columns.ToArray()),
                values,
                "");
        }

        public int Insert(string table, List<string> columns, List<List<object>> values, string extra)
        {
            return Insert(table, String.Join(",", columns.ToArray()), values, extra);
        }

        public int Insert(string table, string columns, List<List<object>> values, string extra)
        {
            RowsUpdated = 0;

            if (table.Length > 0)
            {
                if (values.Count > 0)
                {
                    Cmd = new SQLiteCommand();
                    StringBuilder query = new StringBuilder("INSERT INTO ");
                    query.Append(table);

                    if (columns.Length > 0)
                    {
                        query.AppendFormat(" ({0})", columns);
                    }

                    query.Append(" VALUES ");
                    int index = 0;
                    StringBuilder rowValues = null;

                    foreach (List<object> row in values)
                    {
                        query.Append("(");
                        rowValues = new StringBuilder();

                        foreach (object value in row)
                        {
                            string pname = string.Format("@Param{0}", index);
                            rowValues.AppendFormat("{0},", pname);
                            Cmd.Parameters.Add(new SQLiteParameter(pname, value));
                            index++;
                        }

                        query.AppendFormat("{0}),", rowValues.ToString().Trim(','));
                        rowValues = null;
                    }

                    query = new StringBuilder(query.ToString().Trim(','));

                    if (extra.Length > 0)
                        query.Append(extra);

                    Cmd.Connection = Connect();
                    Cmd.CommandText = query.ToString();
                    log.Debug("Insert query: {0}", Cmd.CommandText);
                    LogSqlParameters();

                    try
                    {
                        RowsUpdated = Cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {
                        log.Error("An error ocurred while trying to insert data: {0}", e);
                        RowsUpdated = -1;
                    }

                    Close();
                }
            }
            else
            {
                log.Error("Missing table name!");
            }

            return RowsUpdated;
        } // End Insert

        #endregion Insert

        #region Update

        public int Update(string table, Dictionary<string, object> data)
        {
            return Update(table, data, null);
        }

        public int Update(string table, Dictionary<string, object> data, SQLWhere where)
        {
            RowsUpdated = 0;

            if (table.Length > 0)
            {
                if (data.Count > 0)
                {
                    Cmd = new SQLiteCommand();
                    StringBuilder query = new StringBuilder("UPDATE ");
                    query.Append(table);
                    query.Append(" SET ");
                    StringBuilder set = new StringBuilder();
                    int paramNo = 0;

                    foreach (KeyValuePair<string, object> kvp in data)
                    {
                        string paramName = string.Format("@UPDATE{0}", paramNo);
                        set.AppendFormat("{0} = {1},", kvp.Key, paramName);
                        SQLiteParameter p = new SQLiteParameter();
                        p.ParameterName = paramName;
                        p.Value = kvp.Value;
                        Cmd.Parameters.Add(p);
                        p = null;
                        paramNo++;
                    }

                    query.Append(set.ToString().TrimEnd(','));
                    set = null;
                    paramNo = 0;
                    string sqlExp = "";
                    SQLiteParameter[] paramArray = null;

                    if (where != null)
                    {
                        sqlExp = where.GetQueryString();

                        if (sqlExp.Length > 0)
                        {
                            paramArray = where.GetParamsAsArray();
                            query.AppendFormat(" {0}", sqlExp);

                            if (paramArray.Length > 0)
                            {
                                Cmd.Parameters.AddRange(paramArray);
                            }

                            sqlExp = "";
                            paramArray = null;
                        }
                    }
                    else
                    {
                        log.Warn("Updating all rows to a single value!");
                    }

                    Cmd.Connection = Connect();
                    Cmd.CommandText = query.ToString();
                    query = null;
                    log.Debug("Update Query: {0}", Cmd.CommandText);
                    LogSqlParameters();

                    try
                    {
                        RowsUpdated = Cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {
                        log.Error("Update Error: {0}", e);
                    }

                    Close();
                }
                else
                {
                    log.Warn("Update Error: data Dictionary contains {0} elements! Nothing to update.", data.Count);
                }
            }
            else
            {
                log.Error("Update Error: missing table name.");
                RowsUpdated = -1;
            }

            return RowsUpdated;
        }

        #endregion Update

        #region ClearTables

        public int ClearTable(string table)
        {
            return Delete(table, null, true);
        }

        public int ClearTable(string table, bool resetRowId)
        {
            return Delete(table, null, resetRowId);
        }

        #endregion ClearTables

        #region Delete

        public int Delete(string table, SQLWhere where)
        {
            return Delete(table, where, true);
        }

        public int Delete(string table, SQLWhere where, bool resetRowId)
        {
            RowsUpdated = 0;

            if (table.Length > 0)
            {
                try
                {
                    StringBuilder query = new StringBuilder("DELETE FROM ");
                    query.Append(table);
                    Cmd = new SQLiteCommand();
                    bool deleteAll = false;

                    if (where != null && where.GetQueryString().Length > 0)
                    {
                        query.AppendFormat(" {0}", where.GetQueryString());
                        SQLiteParameter[] p = where.GetParamsAsArray();

                        if (p != null && p.Length > 0)
                        {
                            Cmd.Parameters.AddRange(p);
                        }

                        p = null;
                    }
                    else
                    {
                        log.Warn("Deleting all rows of {0}!", table);
                        deleteAll = true;
                    }

                    Cmd.Connection = Connect();
                    Cmd.CommandText = query.ToString();
                    RowsUpdated = Cmd.ExecuteNonQuery();

                    if (RowsUpdated > 0)
                    {
                        if (deleteAll && resetRowId)
                        {
                            query = new StringBuilder("DELETE FROM sqlite_sequence ");
                            query.AppendFormat("WHERE name = '{0}'", table);

                            if (Cmd.ExecuteNonQuery() > 0)
                            {
                                log.Info("Table sequence has been reset.");
                            }
                            else
                            {
                                log.Info("Could not reset table sequence");
                            }
                        }

                        log.Info("Records deleted!");
                    }
                    else
                    {
                        log.Warn("Could not delete table {0} records!", table);
                    }

                    query = null;
                    Close();
                }
                catch (Exception e)
                {
                    log.Error("Could not delete data! Reason: {0}", e);
                }
            }
            else
            {
                log.Error("Table name was empty, so, nothing to delete.");
            }

            return RowsUpdated;
        }

        #endregion Delete

        #region Query

        public DataTable Query(string table)
        {
            return Query(table, "", "", null);
        }

        public DataTable Query(string table, List<string> columns)
        {
            return Query(table, String.Join(",", columns.ToArray()), "", null);
        }

        public DataTable Query(string table, string columns, string join)
        {
            return Query(table, columns, join, null);
        }

        public DataTable Query(string table, string columns, SQLWhere where)
        {
            return Query(table, columns, "", where);
        }

        public DataTable Query(string table, string columns, string join, SQLWhere where)
        {
            return Query(table, columns, join, where, "", "", -1, -1);
        }

        public DataTable Query(string table, string columns, string join, SQLWhere where, int limit, int offset)
        {
            return Query(table, columns, join, where, "", "", limit, offset);
        }

        public DataTable Query(string table, string columns, string join, SQLWhere where, string orderBy, string groupBy, int limit, int offset)
        {
            DataTable r = null;

            if (table.Length > 0)
            {
                Cmd = new SQLiteCommand();
                StringBuilder query = new StringBuilder("SELECT ");

                if (columns.Length > 0)
                    query.Append(columns);
                else
                    query.Append("*");

                query.AppendFormat(" FROM {0}", table);

                if (join != null && join.Length > 0)
                {
                    query.AppendFormat(" {0}", join);
                }

                SQLiteParameter[] paramArray = null;
                string queryStr = "";

                if (where != null)
                {
                    queryStr = where.GetQueryString();

                    if (queryStr.Length > 0)
                    {
                        query.AppendFormat(" {0}", queryStr);
                        paramArray = where.GetParamsAsArray();

                        if (paramArray.Length > 0)
                        {
                            Cmd.Parameters.AddRange(paramArray);
                        }
                    }

                    queryStr = "";
                    paramArray = null;
                }

                if (orderBy != null && orderBy.Length > 0)
                {
                    query.AppendFormat(" {0}", orderBy);
                }

                if (groupBy != null && groupBy.Length > 0)
                {
                    query.AppendFormat(" {0}", groupBy);
                }

                if (limit > 0)
                {
                    query.AppendFormat(" LIMIT {0}", limit);
                }

                if (offset >= 0)
                {
                    query.AppendFormat(" OFFSET {0}", offset);
                }

                Cmd.Connection = Connect();
                Cmd.CommandText = query.ToString();
                log.Debug("Query String: {0}", Cmd.CommandText);
                LogSqlParameters();
                query = null;

                try
                {
                    SQLiteDataReader reader = Cmd.ExecuteReader();
                    r = new DataTable();

                    if (reader.HasRows)
                    {
                        r.Load(reader);
                    }
                    else
                    {
                        log.Warn("Query.SQLiteDataReader has no rows!");
                    }

                    reader.Close();
                }
                catch (SQLiteException e)
                {
                    log.Error("Could not execute query: {0}", e);
                }
                catch (ConstraintException ex)
                {
                    log.Error(ex);
                    if (r != null && r.HasErrors)
                    {
                        log.Error("The following rows have errors:");
                        foreach (DataRow row in r.GetErrors())
                        {
                            if (row.HasErrors)
                            {
                                log.Error(row.RowError);
                            }
                        }
                    }
                }
                finally
                {
                    Close();
                }
            }
            else
            {
                log.Debug("Query Error: Missing table name!");
            }

            return r;
        } // End of Query();

        public long RowId()
        {
            Cmd = new SQLiteCommand();
            Cmd.Connection = Connect();

            try
            {
                Cmd.CommandText = "SELECT last_insert_rowid()";
                return (long)Cmd.ExecuteScalar();
            }
            catch (Exception e)
            {
                log.Error("Could not get the last inserted id: {0}", e);
            }

            Close();

            return 0L;
        }

        public object Scalar(string query)
        {
            try
            {
                Cmd = new SQLiteCommand();
                Cmd.Connection = Connect();
                Cmd.CommandText = query;
                return Cmd.ExecuteScalar();
            }
            catch (Exception e)
            {
                log.Error("Could not execute scalar! Reason: {0}", e);
            }
            finally
            {
                Close();
            }

            return null;
        }

        #endregion Query

        private void LogSqlParameters()
        {
            if (log.IsDebugEnabled)
            {
                if (Cmd != null && Cmd.Parameters.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();

                    foreach (SQLiteParameter param in Cmd.Parameters)
                    {
                        sb.AppendFormat("{0} = {1}, ", param.ParameterName, param.Value);
                    }

                    log.Debug("SQLiteParameters: {0}", sb.ToString().Trim(", ".ToCharArray()));
                    sb = null;
                }
                else
                {
                    log.Debug("No SQLParameters.");
                }
            }
        }

        #region SQLite Helper Classes
        public abstract class SQLExpression
        {
            protected int ParamIndex;
            protected StringBuilder Query;
            protected List<SQLiteParameter> ParamList;
            protected string ParamNameFormat = "@Param{0}";

            public SQLExpression()
                : this("")
            {
            }

            protected SQLExpression(string initial)
            {
                Query = new StringBuilder(initial);
                initial = initial.Replace(" ", "").Trim();

                if (initial.Length <= 0)
                    initial = "Param";

                ParamList = new List<SQLiteParameter>();
                ParamNameFormat = string.Format("@{0}{{0}}", initial);
            }

            internal List<SQLiteParameter> GetParams()
            {
                return ParamList;
            }

            internal SQLiteParameter[] GetParamsAsArray()
            {
                return ParamList.ToArray();
            }

            internal virtual string GetQueryString()
            {
                return Query.ToString();
            }
        }

        public class SQLWhere : SQLExpression
        {
            public SQLWhere()
                : base("WHERE ")
            {
            }

            public SQLWhere Append(string expr, object paramValue)
            {
                object[] tmp = new object[] { paramValue };
                return Append(expr, tmp);
            }

            public SQLWhere Append(string condition)
            {
                Query.Append(condition);
                return this;
            }

            public SQLWhere Append(string condition, object[] paramValues)
            {
                try
                {
                    if (paramValues != null && paramValues.Length > 0)
                    {
                        List<string> paramNames = new List<string>();

                        foreach (object param in paramValues)
                        {
                            string paramName = string.Format(ParamNameFormat, ParamIndex);
                            paramNames.Add(paramName);
                            SQLiteParameter p = new SQLiteParameter();

                            /*if (Regex.IsMatch(param.ToString(), @"^\d+$", RegexOptions.Compiled))
                                p.DbType = DbType.Int64;
                            else
                                p.DbType = DbType.String;*/

                            p.ParameterName = paramName;
                            p.Value = param;
                            ParamList.Add(p);
                            p = null;
                            ParamIndex++;
                        }

                        Query.AppendFormat(condition, paramNames.ToArray());
                        paramNames = null;
                    }
                    else
                    {
                        Query.Append(condition);
                    }
                }
                catch (Exception e)
                {
                    log.Error("An error ocurred while appending param values to Where condition: {0}", e);

                    if (log.IsDebugEnabled)
                    {
                        log.Debug("Condition: {0}, ParamValues:", condition);

                        int i = 0;

                        foreach (object param in paramValues)
                        {
                            log.Debug("Param{0}: {1}", i, param);
                            i++;
                        }
                    }
                }

                return this;
            }

        } // End class SQLWhere

        #endregion SQLite Helper Classes

    } // End class SQLiteWrapper
}
