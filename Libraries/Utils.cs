﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Data;
using System.Windows.Forms;
using System.Drawing;

namespace InventoryLibraries.Util
{
    public static class Utils
    {
        private static string BPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static string BasePath
        {
            get
            {
                return BPath;
            }
        }

        public static bool IsNumber(string data)
        {
            return Regex.IsMatch(data.Trim(), @"^[0-9][\^0-9\.,\s]*$", RegexOptions.Compiled);
        }

        public static StringBuilder AppendNL(StringBuilder sb, string content)
        {
            return sb.AppendFormat("{0}{1}", content, Environment.NewLine);
        }

        public static string AppendNL(string s, string content)
        {
            return new StringBuilder(s).AppendFormat("{0}{1}", content, Environment.NewLine).ToString();
        }

        public static string GetNextNumberForFile(string path, string pattern, int numberPos, params char[] separator)
        {
            int i = 0;

            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path, pattern);

                if (files.Length > 0)
                {
                    foreach (string file in files)
                    {
                        int number = int.Parse(file.Split(separator)[numberPos]);

                        if (number > i)
                            i = number;
                    }
                }
            }

            return i.ToString();
        }

        /// <summary>
        /// Changes the BackColor of the control to Red
        /// </summary>
        /// <param name="control">The control we want to change color</param>
        public static void SetInvalidControl(Control control)
        {
            control.BackColor = Color.Red;
        }

        /// <summary>
        /// Changes the BackColor of the control to LawnGreen
        /// </summary>
        /// <param name="control">The control instance we want to change the color</param>
        public static void SetValidControl(Control control)
        {
            control.BackColor = Color.LawnGreen;
        }

        /// <summary>
        /// Resets the control to its default [fore|back]color.
        /// </summary>
        /// <param name="control">The control to reset colors.</param>
        public static void ResetControlColors(Control control)
        {
            control.BackColor = SystemColors.Window;
            control.ForeColor = SystemColors.WindowText;
        }

        public static void ResetControlValues(Control container, Type[] skipTypes, bool resetColors, params Control[] skip)
        {
            ResetControlValues(
                container.Controls,
                skipTypes,
                resetColors,
                skip);
        }

        public static void ResetControlValues(Control container, bool resetColors, params Control[] skip)
        {
            ResetControlValues(
                container.Controls,
                new Type[] { typeof(Button), typeof(Label) },
                resetColors,
                skip);
        }

        public static void ResetControlValues(Control.ControlCollection controls, Type[] skipTypes, bool resetColors, params Control[] skip)
        {
            foreach (Control control in controls)
            {
                Type type = control.GetType();

                if (skipTypes != null)
                {
                    if (skipTypes.Contains(type))
                        continue;
                }

                if (skip != null && skip.Length > 0)
                {
                    if (skip.Contains(control))
                        continue;
                }

                if (resetColors)
                    Utils.ResetControlColors(control);

                if (type == typeof(ComboBox))
                {
                    ((ComboBox)control).SelectedIndex = 0;
                }
                else if (type == typeof(DateTimePicker))
                {
                    ((DateTimePicker)control).Value = DateTime.Now;
                }
                else if (type == typeof(NumericUpDown))
                {
                    NumericUpDown c = (NumericUpDown)control;
                    c.Value = c.Minimum;
                }
                else if (type == typeof(TextBox))
                {
                    ((TextBox)control).Text = "";
                }
            }
        }

        public static void LogColumns(NLog.Logger log, DataTable dt)
        {
            if (log.IsDebugEnabled)
            {
                StringBuilder sb = new StringBuilder();

                foreach (DataColumn dc in dt.Columns)
                {
                    sb.AppendFormat("{0}, ", dc.ColumnName);
                }

                log.Debug("Columns: {0}", sb.ToString().TrimEnd(", ".ToCharArray()));
            }
        }
    }
}
