﻿namespace InventoryCE.View
{
    partial class MovePallet
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbBcdata = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nDepth = new System.Windows.Forms.NumericUpDown();
            this.nColumn = new System.Windows.Forms.NumericUpDown();
            this.btnSave = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbRow = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbProductName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 20);
            this.label1.Text = "Código Escaneado:";
            // 
            // tbBcdata
            // 
            this.tbBcdata.Location = new System.Drawing.Point(3, 25);
            this.tbBcdata.Name = "tbBcdata";
            this.tbBcdata.ReadOnly = true;
            this.tbBcdata.Size = new System.Drawing.Size(127, 23);
            this.tbBcdata.TabIndex = 1;
            this.tbBcdata.TabStop = false;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 20);
            this.label2.Text = "----------------------------------------------------------------";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.nDepth);
            this.panel1.Controls.Add(this.nColumn);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cbRow);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbProductName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(3, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(215, 198);
            // 
            // nDepth
            // 
            this.nDepth.Location = new System.Drawing.Point(77, 133);
            this.nDepth.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nDepth.Name = "nDepth";
            this.nDepth.Size = new System.Drawing.Size(100, 24);
            this.nDepth.TabIndex = 11;
            // 
            // nColumn
            // 
            this.nColumn.Location = new System.Drawing.Point(77, 103);
            this.nColumn.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nColumn.Name = "nColumn";
            this.nColumn.Size = new System.Drawing.Size(100, 24);
            this.nColumn.TabIndex = 10;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSave.Location = new System.Drawing.Point(3, 163);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(209, 31);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Guardar Información";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(3, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 20);
            this.label7.Text = "Estante:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.Text = "Cuerpo:";
            // 
            // cbRow
            // 
            this.cbRow.Location = new System.Drawing.Point(77, 70);
            this.cbRow.Name = "cbRow";
            this.cbRow.Size = new System.Drawing.Size(100, 23);
            this.cbRow.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 20);
            this.label5.Text = "Rack:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 20);
            this.label4.Text = "Ubicación en Bodega";
            // 
            // tbProductName
            // 
            this.tbProductName.Location = new System.Drawing.Point(3, 23);
            this.tbProductName.Name = "tbProductName";
            this.tbProductName.ReadOnly = true;
            this.tbProductName.Size = new System.Drawing.Size(124, 23);
            this.tbProductName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 20);
            this.label3.Text = "Nombre Producto:";
            // 
            // MovePallet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbBcdata);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MovePallet";
            this.Text = "Mover Pallet";
            this.TopMost = true;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBcdata;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbProductName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbRow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.NumericUpDown nDepth;
        private System.Windows.Forms.NumericUpDown nColumn;
    }
}