﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using InventoryCE.Controller;
using InventoryCE.Model;
using NLog;
using SYSCTL;

namespace InventoryCE.View
{
    public partial class Main : Form
    {
        #region Members

        //public static string SDCardPath;
        private static Logger log = LogManager.GetLogger("MainForm");
        private string _bcData = "";
        public static bool Pending = false;
        public static XElement xdoc = null;
        private BarcodeReader reader;
        private Splash splash = null;

        //public static Theme theme = new Theme();

        public const int PROGRESS_MIN = 0;
        public const int PROGRESS_MAX = 100;
        public static string SDCardPath { get; set; }

        public static bool SkipLogin
        {
            get
            {
                return skipLogin;
            }
        }
        private static bool skipLogin = false;

        #endregion Members

        #region Constructors

        public Main(string[] args)
        {
            /*SetSDCard();

            if(SDCardPath.Length <= 0)
                Application.Exit();*/
            splash = new Splash();
            splash.Owner = this;
            splash.Show();
            Application.DoEvents();

            this.Enabled = false;
            this.Width = Program.Width;
            this.Height = Program.Height;

            if (args != null && args.Length >= 1)
            {
                log.Trace("Args length >= 1. Args[0] = {0}", args[0]);

                if (args[0].CompareTo("-skipLogin") == 0)
                {
                    skipLogin = true;
                }
            }

            InitializeComponent();
        }

        #endregion Constructors

        #region Event Handlers

        private void Main_Load(object sender, EventArgs e)
        {
            OnFormLoad();
        }

        private void Main_Closing(object sender, CancelEventArgs e)
        {
            OnFormClose();
        }

        private void btnNewProduct_Click(object sender, EventArgs e)
        {
            OnNewProductClick();
        }

        private void btnExtractProduct_Click(object sender, EventArgs e)
        {
            _bcData = "";

            if (ScanCode())
            {
                log.Debug("Scanned barcode: {0}", _bcData);

                if (_bcData.Length > 0)
                {
                    OnProductExtraction();
                }
                else
                {
                    MessageBox.Show("El código escaneado no es válido");
                }
            }
        }

        private void btnMovePallet_Click(object sender, EventArgs e)
        {
            _bcData = "";

            if (ScanCode())
            {
                log.Debug("Scanned barcode: {0}", _bcData);

                if (_bcData.Length > 0)
                {
                    OnMovePalletClick();
                }
                else
                {
                    MessageBox.Show("El código escaneado no es válido");
                }
            }
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            OnInventoryClick();
        }

        #endregion Event Handlers

        #region Event Processing

        private void OnFormLoad()
        {
            if (CheckSession())
            {
                reader = new BarcodeReader();
                scannerOn.Enabled = reader.EnablePower == false;
                scannerOff.Enabled = reader.EnablePower == true;

                if (SessionController.CurrentUser.Can("change_bcreader_power"))
                {
                    scannerMenu.Enabled = true;
                }
                else
                {
                    scannerMenu.Enabled = false;
                }

                CheckEntriesFile();
                this.Enabled = true;
                this.Visible = true;
                splash.Close();
                splash.Dispose();
                splash = null;
            }
            else
            {
                Application.Exit();
            }
        }

        private void OnFormClose()
        {
            bool discard = false;

            if (Pending)
            {
                DialogResult result = MessageBox.Show(
                    "¡Aún existe información sin guardar! ¿Esta seguro de que desea descartar los que ha hecho? " +
                    "Presione Yes para descartar los cambios o No para volver y revisarlos", "Cambios pendientes", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                discard = result == DialogResult.Yes;
            }

            if (!discard)
            {
                try
                {
                    if (SessionController.IsLoggedIn)
                        SessionController.Logout(DateTime.Now);

                    StoreXmlData();
                    xdoc = null;
                }
                catch (Exception e)
                {
                    log.Error("An error ocurred closing the Main form: {0}", e);
                }
            }
        }

        private void OnNewProductClick()
        {
            ProductEntry pe = null;

            try
            {
                ChangeControlsState(false);
                pe = new ProductEntry();
                pe.Closed += new EventHandler(OnChildFormClose);
                pe.Show();
            }
            catch (Exception ex)
            {
                log.Error("Failed to open Product Entry form: {0}", ex);
            }
            finally
            {
                ChangeControlsState(true);

                if (pe != null)
                {
                    pe.Closed -= OnChildFormClose;
                }
            }
        }

        private void OnMovePalletClick()
        {
            MovePallet mp = null;

            try
            {
                ChangeControlsState(false);
                mp = new MovePallet(_bcData);
                mp.Closed += new EventHandler(OnChildFormClose);
                mp.Show();
            }
            catch (Exception e)
            {
                ChangeControlsState(true);
                log.Error("Failed to open Move Pallet form: {0}", e);
                if (mp != null)
                    mp.Closed -= OnChildFormClose;
            }
        }

        private void OnProductExtraction()
        {
            ProductExtraction pe = null;

            try
            {
                ChangeControlsState(false);
                pe = new ProductExtraction(_bcData);
                pe.Closed += new EventHandler(OnChildFormClose);
                pe.Show();
            }
            catch (Exception e)
            {
                ChangeControlsState(true);
                log.Error("Failed to open Product Extraction form: {0}", e);

                if (pe != null)
                    pe.Closed -= OnChildFormClose;
            }
        }

        private void OnInventoryClick()
        {
            Inventory iv = null;

            try
            {
                ChangeControlsState(false);
                iv = new Inventory();
                iv.Closed += new EventHandler(OnChildFormClose);

                if (!iv.IsDisposed)
                {
                    iv.Show();
                }
                else
                {
                    iv.Closed -= OnChildFormClose;
                    iv = null;
                }
            }
            catch (Exception e)
            {
                ChangeControlsState(true);
                log.Error("Failed to open Inventory form: {0}", e);

                if (iv != null)
                    iv.Closed -= OnChildFormClose;
            }
        }

        private void OnChildFormClose(object sender, EventArgs e)
        {
            ChangeControlsState(true);
            Form f = (Form)sender;
            f.Closed -= OnChildFormClose;
            f.Dispose();
            f = null;

            if (!StoreXmlData())
            {
                MessageBox.Show("Ocurrió un error intentando guardar la información. Revise el registro de errores y contacte con el soporte técnico.", "Error de Escritura");
            }
        }

        private bool ScanCode()
        {
            BCScan bcscan = new BCScan();
            DialogResult result = bcscan.ShowDialog();

            if (result == DialogResult.OK)
            {
                _bcData = bcscan.Barcode;
                bcscan.Dispose();
                return true;
            }

            return false;
        }

        #endregion Event Processing

        #region Utility Functions

        private bool CheckSession()
        {
            if (!SessionController.IsLoggedIn)
            {
                Login login = new Login();
                DialogResult result = login.ShowDialog();

                if (!login.IsDisposed)
                    login.Dispose();

                login = null;

                if (result == DialogResult.OK)
                {
                    ChangeControlsState(true);
                    return true;
                }
            }

            return false;
        }

        private void ChangeControlsState(bool state)
        {
            btnMovePallet.Enabled = state;
            btnNewProduct.Enabled = state;
            btnInventory.Enabled = state;
            btnExtractProduct.Enabled = state;
        }

        public static bool WriteDataToExternalStorage(string data)
        {
            return WriteDataToExternalStorage(data, "internalData.csv", true);
        }

        public static bool WriteDataToExternalStorage(string data, bool overwrite)
        {
            return WriteDataToExternalStorage(data, "internalData.csv", overwrite);
        }

        /// <summary>
        /// Writes the contents of data to the file specified by fileName.
        /// </summary>
        /// <param name="data">The data to be written to the file.</param>
        /// <param name="fileName">Path to the file. It must be a path inside settings.externalStoragePath.</param>
        /// <param name="overwrite">If set to true, then the data is appended, else it's overwritten.</param>
        /// <returns>True if the content was created; false otherwise.</returns>
        public static bool WriteDataToExternalStorage(string data, string fileName, bool overwrite)
        {
            log.Debug("WriteDataToExternalStorage({0}[truncated], {1}, {2})", data.Substring(0, 10), fileName, overwrite);
            string extStorage = Configuration.ExternalStoragePath;

            try
            {
                if (Directory.CreateDirectory(extStorage).Exists)
                {
                    string folder = Path.Combine(extStorage, Path.GetDirectoryName(fileName));

                    if (folder.Length > 0)
                    {
                        if (!Directory.CreateDirectory(folder).Exists)
                        {
                            log.Error("Could not create folder structure!");
                            return false;
                        }
                    }

                    folder = "";
                    string path = Path.Combine(extStorage, fileName);
                    StreamWriter writer = null;

                    if (File.Exists(path) && overwrite)
                        writer = new StreamWriter(path, false, Encoding.UTF8);
                    else
                        writer = new StreamWriter(path, true, Encoding.UTF8);

                    writer.Write(data);
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                    return true;
                }
                else
                {
                    MessageBox.Show("No se encontró la tarjeta SD o no se pudieron crear los sub-directorios!");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("No se pudo guardar la información en el archivo " + fileName + ". Revise el registro de errores.");
                log.Error("Could not write to file {0}: {1}", fileName, e);
            }

            return false;
        }

        /// <summary>
        /// Retrieves a line based on the line number parameter.
        /// </summary>
        /// <param name="fromFile">Path to the file. It must be a folder inside settings.externalStoragePath.</param>
        /// <param name="lineNumber">0 based line number.</param>
        /// <returns>The line found or an empty string if the line was not found (or empty). It's a trimmed string, so, no need to call Trim().</returns>
        public static string ReadLineNumber(string fromFile, int lineNumber)
        {
            int lineNo = 0;
            string line = "";

            try
            {
                string path = Path.Combine(Configuration.ExternalStoragePath, fromFile);

                if (File.Exists(path))
                {
                    StreamReader sr = new StreamReader(path);
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (lineNo == lineNumber)
                            break;

                        lineNo++;
                    }

                    sr.Close();
                    sr = null;
                }
            }
            catch (Exception e)
            {
                log.Error("Could not read line number {0} from file {1}: {2}", lineNumber, fromFile, e);
                log.Debug("Last line number read: {0}.", lineNo);
                log.Debug("Last line read: {0}.", line);
            }

            return line.Trim();
        }

        /// <summary>
        /// Checks if the entries XML file exists or not. If it exists, it loads the data on object xdoc.
        /// Otherwise it creates the main elements.
        /// </summary>
        private void CheckEntriesFile()
        {
            string fileName = Configuration.StorageFileName;
            string path = Path.Combine(Configuration.ExternalStoragePath, fileName);
            bool create = true;

            if (File.Exists(path))
            {
                log.Debug("XML file exists; trying to load data...");
                try
                {
                    xdoc = XElement.Load(path);
                    create = false;
                }
                catch (Exception e)
                {
                    log.Error("Failed to load xml: {0}", e);
                    DialogResult result = MessageBox.Show(
                        "No se pudo cargar el archivo de ingresos.\nSi no se ha guardado la información en el computador " +
                        "los datos se perderán, por lo que tendrá que ingresar toda la información nuevamente.\n" +
                        "Presione YES para descartar la información anterios o NO para cerrar el programa y no perder la información.\n" +
                        "Si no esta seguro presione NO.",
                        "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                    if (result == DialogResult.No)
                    {
                        Close();
                    }
                }
            }

            if (create)
            {
                xdoc = new XElement(
                    new XElement("Content",
                        new XElement("Lots"),
                        new XElement("PalletMoves"),
                        new XElement("PalletModifications"),
                        new XElement("InventoryItems")));
            }
        }

        #endregion Utility Functions

        private void scannerOn_Click(object sender, EventArgs e)
        {
            reader.EnablePower = true;
            scannerOff.Enabled = true;
            scannerOn.Enabled = false;
        }

        private void scannerOff_Click(object sender, EventArgs e)
        {
            reader.EnablePower = false;
            scannerOff.Enabled = false;
            scannerOn.Enabled = true;
        }

        private void fileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool StoreXmlData()
        {
            string path = "";
            try
            {
                if (xdoc != null)
                {
                    path = Path.Combine(Configuration.ExternalStoragePath, Configuration.StorageFileName);

                    if (File.Exists(path))
                    {
                        try
                        {
                            File.Copy(path, Path.Combine(Configuration.ExternalStoragePath, "xmlDataBackup.xml"), true);
                        }
                        catch (Exception ex)
                        {
                            log.Warn("Failed to backup XML data: {0}", ex);
                        }
                    }

                    log.Info("Saving xml data to file {0}.", path);
                    xdoc.Save(path);
                    path = "";
                    return true;
                }
                else
                {
                    log.Debug("XDoc was null, so there was nothing to save.");
                }
            }
            catch (Exception ex)
            {
                log.Error("Failed to save XML data to file {0}: {1}", path, ex);
            }

            return false;
        }

        /// <summary>
        /// Unused. Sets the SD card path to the first removable disk present on the device.
        /// </summary>
        private void SetSDCard()
        {
            try
            {
                string firstCard = "";

                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo("\\");
                System.IO.FileSystemInfo[] fsi = di.GetFileSystemInfos();

                // Iterate through root elements.
                for (int x = 0; x < fsi.Length; x++)
                {
                    // Check to see if this is a temporary storage card (e.g. SD card)
                    if ((fsi[x].Attributes & System.IO.FileAttributes.Temporary) == System.IO.FileAttributes.Temporary)
                    {
                        //if so, return the path
                        firstCard = fsi[x].FullName;
                    }
                }

                SDCardPath = firstCard;
                firstCard = "";
                di = null;
                fsi = null;
            }
            catch (Exception e)
            {
                MessageBox.Show("No se pudo obtener una ruta hacia la tarjeta SD! Asegúrese de que está correctamente montada.");
                log.Error("Could not get SD Card path: {0}", e);
            }
        }
    }
}