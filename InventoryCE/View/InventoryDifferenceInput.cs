﻿using System;
using System.Windows.Forms;
using InventoryCE.Model.DbModel;
using NLog;

namespace InventoryCE.View
{
    public partial class InventoryDifferenceInput : Form
    {
        private TwContainer _container = null;
        private TwInventory _inventory = null;
        private TwInventory.Difference _diffName = TwInventory.Difference.None;
        private int _diffCount = 0;
        private const string _format = "Hay un{0} {1} de {2} rollos.";
        private const string _engFormat = "There was a{0} {1} of {2} rolls on container with lot code {3} and identifier {4}.";
        private Logger log = LogManager.GetLogger("InventoryDifferenceInput");

        public TwInventory Inventory
        {
            get
            {
                return _inventory;
            }
        }

        public InventoryDifferenceInput(TwContainer container)
        {
            InitializeComponent();
            _container = container;
            _inventory = null;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            OnBtnOkClick();
        }

        private void OnBtnOkClick()
        {
            try
            {
                _inventory = new TwInventory();
                _inventory.DifferenceName = _diffName;
                _inventory.DifferenceCount = _diffCount;
                _inventory.Container = _container;
                string comments = "";

                if (_diffName == TwInventory.Difference.Surplus)
                {
                    comments = string.Format(_engFormat,
                        "",
                        _inventory.DifferenceName.ToString(),
                    _inventory.DifferenceCount.ToString(),
                    _container.LotCode,
                    _container.Identifier);
                }
                else if (_diffName == TwInventory.Difference.Ullage)
                {
                    comments = string.Format(_engFormat,
                        "n",
                        _inventory.DifferenceName.ToString(),
                    _inventory.DifferenceCount.ToString(),
                    _container.LotCode,
                    _container.Identifier);
                }
                else
                {
                    comments = "There were no differences.";
                }

                _inventory.Comments = comments;
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception e)
            {
                log.Error("Failed to create Inventory object: ", e);
                MessageBox.Show("Ocurrió un error procesando la información. Revise el registro de errores para obtener más información.", "Error");
                DialogResult = DialogResult.Cancel;
            }
        }

        private void UpdateDifference()
        {
            try
            {
                log.Debug("UpdateDifference() --->");
                lblComment.Visible = false;
                lblComment.Text = "";
                int itemCount = _container.ItemCount;
                int d = Convert.ToInt32(numNewRollCount.Value);
                log.Debug("Value of d: {0}", d);

                if (d > itemCount)
                {
                    _diffCount = d - itemCount;
                    _diffName = TwInventory.Difference.Surplus;
                    lblComment.Text = string.Format(_format, "", "EXCEDENTE", _diffCount);
                }
                else if (d < itemCount)
                {
                    _diffCount = itemCount - d;
                    _diffName = TwInventory.Difference.Ullage;
                    lblComment.Text = string.Format(_format, "a", "MERMA", _diffCount);
                }
                else
                {
                    _diffCount = 0;
                    _diffName = TwInventory.Difference.None;
                    lblComment.Text = "No hay diferencia (está cuadrado).";
                }

                lblComment.Visible = true;
            }
            catch (Exception e)
            {
                log.Error("Failed to update difference: {0}", e);
            }
        }

        private void numNewRollCount_ValueChanged(object sender, EventArgs e)
        {
            UpdateDifference();
        }
    }
}