﻿namespace InventoryCE.View
{
    partial class ProductExtraction
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbBarcode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbProductName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbItemCount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPerItemAvgLength = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.nItemToDraw = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbWarehouse = new System.Windows.Forms.ComboBox();
            this.lblMaxRoll = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cbRow = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.nColumn = new System.Windows.Forms.NumericUpDown();
            this.nDepth = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 32);
            this.label1.Text = "Código Escaneado:";
            // 
            // tbBarcode
            // 
            this.tbBarcode.Location = new System.Drawing.Point(103, 8);
            this.tbBarcode.Name = "tbBarcode";
            this.tbBarcode.ReadOnly = true;
            this.tbBarcode.Size = new System.Drawing.Size(93, 23);
            this.tbBarcode.TabIndex = 1;
            this.tbBarcode.TabStop = false;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 20);
            this.label2.Text = "----------------------------------------------------------";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.Text = "Materia Prima:";
            // 
            // tbProductName
            // 
            this.tbProductName.Location = new System.Drawing.Point(103, 68);
            this.tbProductName.Name = "tbProductName";
            this.tbProductName.ReadOnly = true;
            this.tbProductName.Size = new System.Drawing.Size(93, 23);
            this.tbProductName.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 22);
            this.label4.Text = "Detalles del Pallet";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 20);
            this.label5.Text = "Quedan";
            // 
            // tbItemCount
            // 
            this.tbItemCount.Location = new System.Drawing.Point(61, 130);
            this.tbItemCount.Name = "tbItemCount";
            this.tbItemCount.ReadOnly = true;
            this.tbItemCount.Size = new System.Drawing.Size(66, 23);
            this.tbItemCount.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(133, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.Text = "Rollos.";
            // 
            // tbPerItemAvgLength
            // 
            this.tbPerItemAvgLength.Location = new System.Drawing.Point(61, 167);
            this.tbPerItemAvgLength.Name = "tbPerItemAvgLength";
            this.tbPerItemAvgLength.ReadOnly = true;
            this.tbPerItemAvgLength.Size = new System.Drawing.Size(66, 23);
            this.tbPerItemAvgLength.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(3, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 20);
            this.label7.Text = "Hay";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(133, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 30);
            this.label8.Text = "Mts Promedio.";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(3, 224);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(161, 20);
            this.label9.Text = "¿Cuántos rollos extraerá?:";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(3, 204);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(214, 20);
            this.label10.Text = "Información del Movimiento";
            // 
            // nItemToDraw
            // 
            this.nItemToDraw.Location = new System.Drawing.Point(3, 247);
            this.nItemToDraw.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nItemToDraw.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nItemToDraw.Name = "nItemToDraw";
            this.nItemToDraw.Size = new System.Drawing.Size(100, 24);
            this.nItemToDraw.TabIndex = 14;
            this.nItemToDraw.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nItemToDraw.ValueChanged += new System.EventHandler(this.nItemToDraw_ValueChanged);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(3, 276);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 20);
            this.label11.Text = "Nueva Ubicación";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(3, 302);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 20);
            this.label13.Text = "Bodega Destino:";
            // 
            // cbWarehouse
            // 
            this.cbWarehouse.Location = new System.Drawing.Point(3, 325);
            this.cbWarehouse.Name = "cbWarehouse";
            this.cbWarehouse.Size = new System.Drawing.Size(214, 23);
            this.cbWarehouse.TabIndex = 19;
            // 
            // lblMaxRoll
            // 
            this.lblMaxRoll.Location = new System.Drawing.Point(117, 251);
            this.lblMaxRoll.Name = "lblMaxRoll";
            this.lblMaxRoll.Size = new System.Drawing.Size(100, 20);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSave.Location = new System.Drawing.Point(2, 499);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(214, 35);
            this.btnSave.TabIndex = 30;
            this.btnSave.Text = "Guardar Información";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(3, 351);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(214, 17);
            this.label12.Text = "Ubicación de Destino (Opcional):\r\n";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(3, 413);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 20);
            this.label14.Text = "Rack:";
            // 
            // cbRow
            // 
            this.cbRow.Location = new System.Drawing.Point(63, 410);
            this.cbRow.Name = "cbRow";
            this.cbRow.Size = new System.Drawing.Size(100, 23);
            this.cbRow.TabIndex = 47;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(3, 443);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 20);
            this.label15.Text = "Cuerpo:";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(3, 473);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 20);
            this.label16.Text = "Estante:";
            // 
            // nColumn
            // 
            this.nColumn.Location = new System.Drawing.Point(63, 439);
            this.nColumn.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nColumn.Name = "nColumn";
            this.nColumn.Size = new System.Drawing.Size(100, 24);
            this.nColumn.TabIndex = 50;
            // 
            // nDepth
            // 
            this.nDepth.Location = new System.Drawing.Point(63, 469);
            this.nDepth.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nDepth.Name = "nDepth";
            this.nDepth.Size = new System.Drawing.Size(100, 24);
            this.nDepth.TabIndex = 51;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(4, 368);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(173, 39);
            this.label17.Text = "No elija ningún rack si es que el contenido no será movido a una ubicación especí" +
                "fica.";
            // 
            // ProductExtraction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.nDepth);
            this.Controls.Add(this.nColumn);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cbRow);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cbWarehouse);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblMaxRoll);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.nItemToDraw);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbPerItemAvgLength);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbItemCount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbProductName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbBarcode);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductExtraction";
            this.Text = "Extraer Rollos";
            this.TopMost = true;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ProductExtraction_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBarcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbProductName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbItemCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbPerItemAvgLength;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nItemToDraw;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbWarehouse;
        private System.Windows.Forms.Label lblMaxRoll;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbRow;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nColumn;
        private System.Windows.Forms.NumericUpDown nDepth;
        private System.Windows.Forms.Label label17;
    }
}