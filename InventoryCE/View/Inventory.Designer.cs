﻿namespace InventoryCE.View
{
    partial class Inventory
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbProductName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbItemCount = new System.Windows.Forms.TextBox();
            this.tbMtrsPerRoll = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSquare = new System.Windows.Forms.Button();
            this.btnDiff = new System.Windows.Forms.Button();
            this.btnInventoryEnd = new System.Windows.Forms.Button();
            this.btnContinue = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbProductName
            // 
            this.tbProductName.Location = new System.Drawing.Point(103, 91);
            this.tbProductName.Name = "tbProductName";
            this.tbProductName.ReadOnly = true;
            this.tbProductName.Size = new System.Drawing.Size(93, 23);
            this.tbProductName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.Text = "Materia Prima:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 20);
            this.label2.Text = "----------------------------------------------------------";
            // 
            // tbBarcode
            // 
            this.tbBarcode.Location = new System.Drawing.Point(103, 4);
            this.tbBarcode.Name = "tbBarcode";
            this.tbBarcode.ReadOnly = true;
            this.tbBarcode.Size = new System.Drawing.Size(93, 23);
            this.tbBarcode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 32);
            this.label1.Text = "Código Escaneado:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 20);
            this.label5.Text = "No. de Rollos:";
            // 
            // tbItemCount
            // 
            this.tbItemCount.Location = new System.Drawing.Point(103, 120);
            this.tbItemCount.Name = "tbItemCount";
            this.tbItemCount.ReadOnly = true;
            this.tbItemCount.Size = new System.Drawing.Size(93, 23);
            this.tbItemCount.TabIndex = 2;
            // 
            // tbMtrsPerRoll
            // 
            this.tbMtrsPerRoll.Location = new System.Drawing.Point(103, 149);
            this.tbMtrsPerRoll.Name = "tbMtrsPerRoll";
            this.tbMtrsPerRoll.ReadOnly = true;
            this.tbMtrsPerRoll.Size = new System.Drawing.Size(93, 23);
            this.tbMtrsPerRoll.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 20);
            this.label6.Text = "Mtrs. por Rollo:";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(232, 20);
            this.label4.Text = "Información Materia Prima";
            // 
            // btnSquare
            // 
            this.btnSquare.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnSquare.Enabled = false;
            this.btnSquare.ForeColor = System.Drawing.Color.White;
            this.btnSquare.Location = new System.Drawing.Point(3, 178);
            this.btnSquare.Name = "btnSquare";
            this.btnSquare.Size = new System.Drawing.Size(72, 32);
            this.btnSquare.TabIndex = 4;
            this.btnSquare.Text = "Cuadrado";
            this.btnSquare.Click += new System.EventHandler(this.btnSquare_Click);
            // 
            // btnDiff
            // 
            this.btnDiff.BackColor = System.Drawing.Color.OrangeRed;
            this.btnDiff.Enabled = false;
            this.btnDiff.ForeColor = System.Drawing.Color.White;
            this.btnDiff.Location = new System.Drawing.Point(163, 178);
            this.btnDiff.Name = "btnDiff";
            this.btnDiff.Size = new System.Drawing.Size(72, 32);
            this.btnDiff.TabIndex = 5;
            this.btnDiff.Text = "Diferencia";
            this.btnDiff.Click += new System.EventHandler(this.btnDiff_Click);
            // 
            // btnInventoryEnd
            // 
            this.btnInventoryEnd.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnInventoryEnd.Enabled = false;
            this.btnInventoryEnd.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnInventoryEnd.Location = new System.Drawing.Point(3, 216);
            this.btnInventoryEnd.Name = "btnInventoryEnd";
            this.btnInventoryEnd.Size = new System.Drawing.Size(232, 31);
            this.btnInventoryEnd.TabIndex = 7;
            this.btnInventoryEnd.Text = "Finalizar Inventario";
            this.btnInventoryEnd.Click += new System.EventHandler(this.btnInventoryEnd_Click);
            // 
            // btnContinue
            // 
            this.btnContinue.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnContinue.Enabled = false;
            this.btnContinue.Location = new System.Drawing.Point(81, 178);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(76, 32);
            this.btnContinue.TabIndex = 6;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // Inventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 254);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.btnInventoryEnd);
            this.Controls.Add(this.btnDiff);
            this.Controls.Add(this.btnSquare);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbMtrsPerRoll);
            this.Controls.Add(this.tbItemCount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbProductName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbBarcode);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Inventory";
            this.Text = "Inventariar";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbProductName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbItemCount;
        private System.Windows.Forms.TextBox tbMtrsPerRoll;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSquare;
        private System.Windows.Forms.Button btnDiff;
        private System.Windows.Forms.Button btnInventoryEnd;
        private System.Windows.Forms.Button btnContinue;
    }
}