﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using InventoryCE.Controller;
using InventoryCE.Model;
using InventoryCE.Model.DbModel;
using InventoryLibraries.SQLite;
using InventoryLibraries.Util;
using NLog;

namespace InventoryCE.View
{
    public partial class AddPalletView : Form
    {
        private Logger log = LogManager.GetLogger("PalletEntry");
        private int _palletId = 1;

        // DB Model
        private SQLiteWrapper _wrap = SQLiteWrapper.GetInstance(Configuration.DBFile);
        private TwContainer _container = null;
        private XElement _locs = null;
        private List<Warehouse> _whList = new List<Warehouse>();

        /// <summary>
        /// Lock for synchronize barcode scans.
        /// </summary>
        private Object lockParser = new Object();

        /// <summary>
        /// Returns the XML Data for the Locations elements.
        /// </summary>
        public XElement GetXml
        {
            get
            {
                return _locs;
            }
        }

        public AddPalletView(TwContainer container, XElement parent, int lastPalletId)
        {
            if (container == null)
                throw new NullReferenceException("The container cannot be null!");

            _container = container;
            _palletId = lastPalletId;
            InitializeComponent();
            FillCBRack();
            SetMaxValues();

            if (parent != null)
                CheckContents(parent);
        }

        #region Utils

        /// <summary>
        /// Checks if the Locations Object exists and stores saves it as a shortcut for later use.
        /// </summary>
        /// <param name="parent">Parent element where the Locations XML data should be.</param>
        private void CheckContents(XElement parent)
        {
            log.Debug("Checking parent contents looking for existing locations...");

            try
            {
                if (parent.Element("Locations") != null)
                {
                    if (parent.Element("Locations").HasElements)
                    {
                        _locs = parent.Element("Locations");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Failed to check if there are locations in the inlet guide xml: {0}", ex);
            }
        }

        /// <summary>
        /// Parses the barcode data.
        /// </summary>
        /// <param name="data">Barcode data.</param>
        private void ParseBarcode(string data)
        {
            String tmp = "";
            tmp = data.Trim();
            log.Debug("ParseBarcode(data: {0}) --->", tmp);
            log.Debug("tbTotalLength.Focused: {0}", tbTotalLength.Focused);
            tmp = tmp.Substring(6);

            if (Utils.IsNumber(tmp))
            {
                lock (lockParser)
                {
                    tbTotalLength.Text = double.Parse(tmp).ToString();
                    OnUpdateRollCount(null, null);
                }
            }
        }

        /// <summary>
        /// Fills the Rack combo box with the values specified in the Configuration file.
        /// </summary>
        private void FillCBRack()
        {
            try
            {
                UIUtils.FillRowCombo(cbLocRow);
            }
            catch (Exception e)
            {
                log.Error("Could not fill rack data: {0}", e);
            }
        }

        /// <summary>
        /// Stores the current XML data to the global XML object.
        /// </summary>
        private void SaveData()
        {
            btnSave.Enabled = false;

            if (ValidateData())
            {
                if (NewPallet())
                    ResetControls();
            }
            else
            {
                MessageBox.Show("No ha llenado todos los valores. Revise la información y corríjala (Rojo = mal, Verde = bien).");
                log.Debug("Invalid data!");
            }

            btnSave.Enabled = true;
        }

        /// <summary>
        /// Adds the new pallet to the current XML data.
        /// </summary>
        /// <returns>True on success or false if the operation fails.</returns>
        private bool NewPallet()
        {
            try
            {
                log.Debug("NewPallet() --->");

                if (_locs == null)
                    _locs = new XElement("Locations", "");

                string col = "";

                if (nLocCol.Value < 10)
                    col = string.Format("0{0}", nLocCol.Value);
                else
                    col = nLocCol.Value.ToString();

                string depth = "";

                if (nLocDepth.Value < 10)
                    depth = string.Format("0{0}", nLocDepth.Value);
                else
                    depth = nLocDepth.Value.ToString();

                Location tmp = LocationExists(cbLocRow.SelectedItem.ToString(), col, depth, false);
                tmp.Warehouse = _container.Location.Warehouse;
                depth = col = "";
                _container.Dimensions = "";
                _container.ItemCount = int.Parse(tbRollCount.Text);
                _container.Location = tmp;
                _container.Modifications = 0;
                _container.ItemTotalLength = double.Parse(tbTotalLength.Text);
                _container.PerItemAvgLength = double.Parse(tbRollLength.Text);
                _container.Identifier = _palletId;
                _palletId++;

                XElement loc = null;

                if (_locs.HasElements)
                {
                    try
                    {
                        IEnumerable<XElement> list = (from l in _locs.Elements()
                                                      where l.Element("Row").Value == tmp.Row &&
                                                        l.Element("Column").Value == tmp.Column &&
                                                        l.Element("Depth").Value == tmp.Depth
                                                      select l);

                        if (list.Any())
                        {
                            loc = list.First();
                        }

                        list = null;
                    }
                    catch (Exception ex)
                    {
                        log.Warn("LINQ query to locations failed: {0}", ex);
                    }
                }

                UserContainer uc = new UserContainer();
                uc.Action = UserContainer.ActionName.Entry;
                uc.ActionDate = DateTime.Now;
                uc.Container = _container;
                uc.ContainerId = _container.Id;
                uc.UserId = SessionController.CurrentUser.Id;
                uc.User = SessionController.CurrentUser;
                log.Trace("Uc = {0}, _locs = {1}", uc, _locs);

                if (loc == null)
                {
                    try
                    {
                        _locs.Add(tmp.ToXml(_container.ToXml(uc.ToXml())));
                    }
                    catch (NullReferenceException ex)
                    {
                        log.Error("Failed to add containers to xml document: {0}", ex);
                    }
                }
                else
                {
                    try
                    {
                        loc.Element("Containers").Add(_container.ToXml(uc.ToXml()));
                    }
                    catch (NullReferenceException ex)
                    {
                        log.Error("Failed to add containers to xml document: {0}", ex);
                    }
                }

                uc = null;
                loc = null;
                tmp = null;
                log.Debug("_locs: {0}", _locs.ToString());
                MessageBox.Show("Pallet ingresado. Puede seguir ingresando pallets o cerrar la ventana para terminar el ingreso.", "Pallet Guardado");
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error ingresando el pallet: " + e.Message, "Error");
                log.Error("Could not store new pallet: {0}", e);
            }

            return false;
        }

        /// <summary>
        /// Checks if the location exists.
        /// </summary>
        /// <param name="row">Location Row.</param>
        /// <param name="col">Location Column.</param>
        /// <param name="depth">Location Depth.</param>
        /// <param name="create">Specifies if the Location must be created if it doesn't exists.</param>
        /// <returns>A Location object if it was found or null if it doesn't exists.</returns>
        private Location LocationExists(string row, string col, string depth, bool create)
        {
            Location l = new Location();
            l.Row = row;
            l.Column = col;
            l.Depth = depth;
            l.Name = "";
            l.Position = "";
            return LocationExists(l, create);
        }

        private Location LocationExists(Location loc)
        {
            return LocationExists(loc, true);
        }

        private Location LocationExists(Location loc, bool create)
        {
            log.Debug("LocationExists()");
            DataTable dt = null;

            try
            {
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("row = {0} AND depth = {1} AND column = {2}", new object[] {
                    loc.Row, loc.Depth, loc.Column
                });

                dt = _wrap.Query(
                    "Location",
                    "*",
                    where);

                if (dt != null && dt.Rows.Count == 1)
                {
                    log.Debug("Location {0}{1}{2} exists!", loc.Row, loc.Column, loc.Depth);
                    loc.Id = long.Parse(dt.Rows[0]["locationId"].ToString());
                }
                else if (dt != null && dt.Rows.Count > 1)
                {
                    log.Error("There are duplicate positions on the \"location\" table!");
                }
                else
                {
                    if (create)
                    {
                        log.Debug("Trying to store new location");
                        int updated = _wrap.Insert(
                            "Location",
                            "name, position, row, column, depth, warehouseId",
                            new List<List<object>>()
                            {
                                new List<object>()
                                {
                                    loc.Name, loc.Position, loc.Row, loc.Column, loc.Depth, loc.Warehouse.Id
                                }
                            },
                            "");

                        if (updated > 0)
                        {
                            loc.Id = _wrap.RowId();
                            log.Debug("Location {0}{1}{2} creted on warehouse id {3}!", loc.Row, loc.Column, loc.Depth, loc.Warehouse.Id);
                        }
                        else
                        {
                            log.Error("Could not insert location {0}{1}{2}!", loc.Row, loc.Column, loc.Depth);
                        }
                    }

                    log.Debug("Location does not exist!");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("No se pudo verificar que la ubicación exista en la base de datos. Revise el registro de errores para obtener más información.");
                log.Error("Could not verify that the location {0}{1}{2} exists!. Reason: {3}", loc.Row, loc.Column, loc.Depth, e);
                DialogResult = DialogResult.Cancel;
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }

            return loc;
        }

        /// <summary>
        /// Validates user Input.
        /// </summary>
        /// <returns>True if valid. False otherwise.</returns>
        private bool ValidateData()
        {
            bool r = true;

            if (!Utils.IsNumber(tbTotalLength.Text))
            {
                Utils.SetInvalidControl(tbTotalLength);
                r &= false;
            }
            else
            {
                Utils.SetValidControl(tbTotalLength);
            }

            if (cbLocRow.SelectedIndex <= 0)
            {
                Utils.SetInvalidControl(cbLocRow);
                r &= false;
            }
            else
            {
                Utils.SetValidControl(cbLocRow);
            }

            Utils.SetValidControl(tbRollLength);
            Utils.SetValidControl(nLocCol);
            Utils.SetValidControl(nLocDepth);

            return r;
        }

        /// <summary>
        /// Checks if the container with the identifier and lot code exists.
        /// </summary>
        /// <param name="identifier">Identifier to search</param>
        /// <param name="lotCode">The lot code that identifies the container</param>
        /// <returns>Returns 0 if the container does not exists and the last identifier if it does not.</returns>
        private long ContainerExists(long identifier, long lotCode)
        {
            DataTable dt = null;

            try
            {
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("Container.lotCode = {1} AND Container.identifier = {0}", new object[] { identifier, lotCode });
                dt = _wrap.Query(
                    "Container",
                    "identifier",
                    where);
                where = null;

                if (dt != null && dt.Rows.Count == 1)
                {
                    log.Info("Container with Identifier {0} and Lot Code {1} exists.", identifier, lotCode);
                    return long.Parse(dt.Rows[0]["identifier"].ToString());
                }
            }
            catch (Exception e)
            {
                log.Error("Could not determine if container exists: {0}", e);
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }

            return 0L;
        }

        /// <summary>
        /// Sets the max values for the location.column and location.depth.
        /// </summary>
        private void SetMaxValues()
        {
            try
            {
                nLocCol.Maximum = decimal.Parse(Configuration.Rack.MaxColumns);
                nLocDepth.Maximum = decimal.Parse(Configuration.Rack.MaxDepth);
            }
            catch (Exception e)
            {
                log.Error("Error trying to set maximum column and depth values: {0}", e);
            }
            finally
            {
                nLocCol.Maximum = 100;
                nLocDepth.Maximum = 100;
            }
        }

        /// <summary>
        /// Enable form controls and removes controls data.
        /// </summary>
        private void ResetControls()
        {
            try
            {
                log.Debug("ResetControls() --->");
                Utils.ResetControlValues(this, true);
            }
            catch (Exception e)
            {
                log.Error("Could not reset control values: {0}", e);
            }
        }

        #endregion Utils

        #region Event Handlers

        private void UpdateRollCount_ValueChanged(object sender, EventArgs e)
        {
            OnUpdateRollCount(sender, e);
        }

        private void tlScan_Click(object sender, EventArgs e)
        {
            OnBcRead();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void PalletData_Closing(object sender, CancelEventArgs e)
        {
            OnClosing();
        }

        private void cbLocRow_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                log.Debug("Location row change: {0}", cbLocRow.SelectedItem);
                char row = cbLocRow.SelectedItem.ToString().ToLower()[0];

                if (row == 'g')
                {
                    if (nLocCol.Value > 14)
                        nLocCol.Value = 14;

                    if (nLocDepth.Value > 12)
                        nLocDepth.Value = 12;

                    nLocCol.Maximum = 14;
                    nLocDepth.Maximum = 12;
                }
                else
                {
                    nLocCol.Maximum = 99;
                    nLocDepth.Maximum = 99;
                }
            }
            catch (Exception ex)
            {
                log.Error("SelectedIndexChange - Could not set container.location.row: {0}", ex);
            }
        }

        #endregion Event Handlers

        #region OnEvent Handlers

        /// <summary>
        /// Updates the roll count when the total length or roll lenght values changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUpdateRollCount(object check, EventArgs e)
        {
            String totalLength = tbTotalLength.Text,
                rollCount = tbRollCount.Text;

            double dTotalLength = -1;
            int iRollCount = -1;

            if (totalLength != null && totalLength.Length > 0)
            {
                try
                {
                    dTotalLength = Convert.ToDouble(totalLength);
                }
                catch (Exception ex)
                {
                    log.Error("Failed to convert {0} to double.", totalLength);
                    log.Debug(ex);
                }
            }

            if (rollCount != null && rollCount.Length > 0)
            {
                try
                {
                    iRollCount = int.Parse(rollCount);
                }
                catch (Exception ex)
                {
                    log.Error("Failed to parse {0} to integer value.", rollCount);
                    log.Debug(ex);
                }
            }

            if (dTotalLength > 0 && iRollCount >= 0)
            {
                try
                {
                    tbRollLength.Text = (dTotalLength / iRollCount).ToString();
                }
                catch (Exception ex)
                {
                    log.Error("OnUpdateRollCount - Failed to update value:\n{0}", ex);
                }
            }

        }

        /// <summary>
        /// Handles the Closing event.
        /// </summary>
        private void OnClosing()
        {
            tbRollLength.LostFocus -= UpdateRollCount_ValueChanged;
            tbTotalLength.LostFocus -= UpdateRollCount_ValueChanged;
            log = null;
            _wrap = null;
            _container = null;
        }

        /// <summary>
        /// Handles the Barcode scan button click. Creates a new Window where to scan the barcode and parses the input.
        /// </summary>
        private void OnBcRead()
        {
            Regex regex = new Regex(@"^(\(\d+\)\d+){1}$", RegexOptions.Compiled | RegexOptions.Multiline);
            BCScan scan = new BCScan(regex);

            if (scan.ShowDialog() == DialogResult.OK)
            {
                ParseBarcode(scan.Barcode);
            }
            else
            {
                MessageBox.Show("El código escaneado no es válido.", "Código no válido");
            }

            if (!scan.IsDisposed)
                scan.Dispose();

            scan = null;
        }

        #endregion OnEvent Handlers
    }
}