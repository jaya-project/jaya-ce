﻿using System;
using System.Windows.Forms;
using InventoryCE.Controller;
using InventoryCE.Model;
using NLog;

namespace InventoryCE.View
{
    public partial class Login : Form
    {
        private bool Logged = false;
        private Logger log = LogManager.GetLogger("LoginForm");

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Closed(object sender, EventArgs e)
        {
            if (!Logged)
            {
                DialogResult = DialogResult.Abort;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            log.Debug("btnLogin_Click");
            ProcessLogin();
        }

        private void InitializeProgressBar()
        {
            pbLoginProgress.Minimum = 0;
            pbLoginProgress.Maximum = 101;
            pbLoginProgress.Value = 1;
        }

        private void ProcessLogin()
        {
            log.Info("Trying to log in...");
            ChangeControlState(false);
            pbLoginProgress.Visible = true;
            SessionController.UpdateProgress += UpdateProgressBar;
            InitializeProgressBar();
            string name = tbUser.Text,
                pass = tbPassword.Text;
            log.Debug("User {0}, Password {1}", name, pass);

            SessionController.ErrorCode code = SessionController.Authenticate(name, pass);
            string msg = "";

            switch (code)
            {
                case SessionController.ErrorCode.Success:
                    pbLoginProgress.Visible = false;
                    SessionController.UpdateProgress -= UpdateProgressBar;
                    Logged = true;
                    DialogResult = DialogResult.OK;
                    break;

                case SessionController.ErrorCode.InvalidCredentials:
                    msg = "Los datos ingresados no son válidos. Asegúrese de estar utilizando el usuario y clave correctas.";
                    break;

                case SessionController.ErrorCode.SessionNotSaved:
                    msg = "No se pudo guardar los datos de sesión. Por favor, revise los registros de error.";
                    break;

                case SessionController.ErrorCode.None:
                    msg = "Al parecer no se realizó ninguna acción.";
                    break;

                case SessionController.ErrorCode.Error:
                    msg = "Ocurrió un error iniciando sesión. Por favor, revise los registros de error.";
                    break;
            }

            if (msg.Length > 0)
            {
                MessageBox.Show(msg);
                pbLoginProgress.Visible = false;
            }

            SessionController.UpdateProgress -= UpdateProgressBar;
            ChangeControlState(true);
        }

        private void ChangeControlState(bool enabled)
        {
            tbUser.Enabled = enabled;
            tbPassword.Enabled = enabled;
            btnLogin.Enabled = enabled;
        }

        private void UpdateProgressBar(object sender, ProgressArgs e)
        {
            pbLoginProgress.Value = e.Value;
        }

        private void Login_Load(object sender, EventArgs e)
        {
            if (Main.SkipLogin)
            {
                tbUser.Text = "admin";
                tbPassword.Text = "jas3qA";
                btnLogin.Focus();
                btnLogin_Click(btnLogin, EventArgs.Empty);
            }
        }
    }
}