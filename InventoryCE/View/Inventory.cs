﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using InventoryCE.Model;
using InventoryCE.Model.DbModel;
using InventoryLibraries.SQLite;
using NLog;

namespace InventoryCE.View
{
    public partial class Inventory : Form
    {
        private Logger log = LogManager.GetLogger("Inventory");

        private SQLiteWrapper _wrap = SQLiteWrapper.GetInstance(Configuration.DBFile);
        private TwContainer _container = null;
        private TwInventory _inventory = null;
        private bool _started = false;
        private const string _dateFormat = "dd-MM-yyyy H:mm:ss";

        public Inventory()
        {
            InitializeComponent();
            CheckStart();
            btnInventoryEnd.Enabled = _started;
        }

        private void CheckStart()
        {
            if (Main.xdoc.Element("InventoryItems").HasElements)
            {
                XAttribute attr = Main.xdoc.Element("InventoryItems").Element("Inventory").Attribute("EndDate");

                if (attr != null && attr.Value.Length > 0)
                {
                    MessageBox.Show("El inventario ya terminó, por lo que solo puede ir al computador a descargar la información.", "Inventario Finalizado");
                    // Disables all buttons.
                    btnInventoryEnd.Enabled = btnSquare.Enabled = btnDiff.Enabled = btnContinue.Enabled = false;
                }
                else
                {
                    DialogResult result = MessageBox.Show(
                        "Ya comenzó un inventario anteriormente.\n" +
                        "¿Desea continuar con ese inventario o comenzar uno nuevo?\n" +
                        "Presione YES para continuar o NO para comenzar uno nuevo.",
                        "Inventario sin finalizar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                    if (result == DialogResult.Yes)
                    {
                        // Continue with inventory...
                        btnContinue.Text = "Continuar";
                        _started = true;
                        btnInventoryEnd.Enabled = true;
                    }
                    else
                    {
                        Main.xdoc.Element("InventoryItems").Elements().Remove();
                        btnContinue.Text = "Comenzar";
                        _started = false;
                    }

                    btnContinue.Enabled = true;
                }
            }
            else
            {
                _started = false;
                btnContinue.Text = "Comenzar";
                btnContinue.Enabled = true;
            }
        }

        private void OnContinue()
        {
            btnInventoryEnd.Enabled =
                btnContinue.Enabled = false;

            try
            {
                ReadBarcode();
                _container = DecodeBarcode(_container);

                if (_container != null)
                {
                    ReadData();

                    if (!ContainerExists(_container.Id))
                    {
                        if (SetData())
                        {
                            btnDiff.Enabled = btnSquare.Enabled = true;
                            btnContinue.Text = "Continuar";
                        }
                        else
                        {
                            btnContinue.Enabled = true;
                        }
                    }
                    else
                    {
                        tbBarcode.Text = "";
                        MessageBox.Show("El pallet que intenta inventariar ya existe.", "Pallet Duplicado");
                        btnContinue.Enabled = true;
                        btnInventoryEnd.Enabled = _started;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("OnContinue: {0}", e);
                btnContinue.Enabled = true;

                if (btnContinue.Text != "Comenzar")
                    btnContinue.Text = "Comenzar";
            }
        }

        private void ReadBarcode()
        {
            Regex validator = new Regex(Configuration.Barcode.Format);
            BCScan bcscan = new BCScan(validator);
            DialogResult result = bcscan.ShowDialog();

            if (result == DialogResult.OK)
            {
                tbBarcode.Text = bcscan.Barcode;
            }
            else
            {
                log.Debug("Scan failed?");
            }

            if (!bcscan.IsDisposed)
                bcscan.Dispose();
        }

        /// <summary>
        /// Searchs in the XML document to find if a container with the same id and identifier already exists.
        /// </summary>
        /// <param name="id">The TwContainer ID.</param>
        /// <returns>True if it already exists or false otherwise.</returns>
        private bool ContainerExists(long id)
        {
            XElement inventoryElement = null;
            XElement container = null;

            try
            {
                inventoryElement = Main.xdoc.Element("InventoryItems").Element("Inventory");
            }
            catch (Exception ex)
            {
                log.Error("Failed to retrieve inventory XML element. This may be due the inventory has not begun yet: {0}", ex);
            }

            if (inventoryElement != null && inventoryElement.HasElements && inventoryElement.Elements("Item").Count() > 0)
            {
                try
                {
                    container = (from node in inventoryElement.Elements("Item")
                                 where long.Parse(node.Element("ContainerId").Value) == id
                                 select node).First();
                }
                catch (Exception ex)
                {
                    log.Error("Failed to check if the container with id {0} exists: {1}", id, ex);
                }
            }

            return container != null;
        }

        private void ReadData()
        {
            DataTable dt = null;

            try
            {
                log.Debug("ReadData() --->");
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("Container.identifier = {0} AND Container.lotCode = {1}", new object[] { _container.Identifier, _container.LotCode });
                StringBuilder expr = new StringBuilder();
                expr.Append("Container.itemId AS iid, itemName AS iname, Item.itemCode AS icode, ");
                expr.Append("Container.containerId AS cid, Container.lotCode, Container.identifier, itemCount, itemTotalLength, perItemAvgLength, unitPrice, locationId");
                string columns = expr.ToString();
                expr = new StringBuilder();
                expr.AppendFormat("INNER JOIN {0} ON Item.itemId = Container.itemId ", "Item");
                string join = expr.ToString();
                dt = _wrap.Query(
                    "Container",
                    columns,
                    join,
                    where,
                    1,
                    0);
                expr = null;
                columns = "";
                join = "";
                where = null;

                if (dt != null && dt.Rows.Count == 1)
                {
                    DataRow r = dt.Rows[0];
                    _container.Id = StringUtils.GetLong(r["cid"]);
                    _container.LotCode = StringUtils.GetString(r["lotCode"]);
                    _container.Identifier = StringUtils.GetLong(r["identifier"]);
                    _container.ItemCount = StringUtils.GetInt(r["itemCount"]);
                    _container.ItemTotalLength = Convert.ToDouble(StringUtils.GetInt(r["itemTotalLength"]));
                    _container.LocationId = StringUtils.GetLong(r["locationId"]);
                    _container.PerItemAvgLength = StringUtils.GetDouble(r["perItemAvgLength"]);
                    _container.UnitPrice = Convert.ToDouble(StringUtils.GetInt(r["unitPrice"]));
                    _container.Item = new InventoryCE.Model.DbModel.Item();
                    _container.Item.Id = StringUtils.GetLong(r["iid"]);
                    _container.Item.Code = StringUtils.GetString(r["icode"]);
                    _container.Item.Name = StringUtils.GetString(r["iname"]);
                    r = null;
                }
                else
                {
                    MessageBox.Show("No se encontró la materia prima. Quizás escaneó un código equivocado. Inténtelo nuevamente.", "No encontrado");
                    _container = null;
                }
            }
            catch (Exception e)
            {
                log.Error("Error reading container information: {0}", e);
                MessageBox.Show("No se encontró la información del contenedor. Revise el registro de errores para mayor información.");
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }
        }

        private bool SetData()
        {
            if (_container != null)
            {
                log.Debug("SetData() --->");
                tbProductName.Text = _container.Item.Name;
                tbItemCount.Text = _container.ItemCount.ToString();
                tbMtrsPerRoll.Text = _container.PerItemAvgLength.ToString();
                return true;
            }

            return false;
        }

        private void OnBtnDiffClick()
        {
            InventoryDifferenceInput window = null;

            btnDiff.Enabled =
                btnSquare.Enabled = false;

            try
            {
                window = new InventoryDifferenceInput(_container);
                DialogResult r = window.ShowDialog();

                if (r == DialogResult.OK)
                {
                    _inventory = window.Inventory;
                    SetStartDate();

                    if (StoreData())
                    {
                        _inventory = null;
                        _container = null;
                    }
                }
                else
                {
                    btnDiff.Enabled =
                        btnSquare.Enabled = true;
                }

                if (!window.IsDisposed)
                {
                    window.Dispose();
                }
            }
            catch (Exception e)
            {
                log.Error("Failed to open InventoryDifferenceInput: {0}", e);
                MessageBox.Show("Ocurrió un error intentando abrir la ventana. Revise el registro de errores.", "Error");

                if (window != null)
                {
                    if (!window.IsDisposed)
                        window.Dispose();
                }

                btnDiff.Enabled =
                    btnSquare.Enabled = true;
            }
        }

        private void OnBtnSquareClick()
        {
            btnDiff.Enabled =
                btnSquare.Enabled = false;
            try
            {
                _inventory = new TwInventory();
                _inventory.Container = _container;
                _inventory.DifferenceCount = 0;
                _inventory.DifferenceName = TwInventory.Difference.None;
                _inventory.Comments = "There was no difference.";
                SetStartDate();
                if (StoreData())
                {
                    _inventory = null;
                    _container = null;
                }
                else
                {
                    log.Error("Failed to store data!");
                }
            }
            catch (Exception e)
            {
                log.Error("Failed to create TwInventory object: {0}", e);
                btnDiff.Enabled =
                    btnSquare.Enabled = true;
            }

        }

        private void OnBtnInventoryEndClick()
        {
            DialogResult result = MessageBox.Show(
                "¿Está seguro de que desea finalizar el inventario?\n" +
                "Presione OK para finalizarlo o haga clic sobre la X para cancelar y continuar con el inventario.",
                "Finalizar Inventario", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

            if (result == DialogResult.OK)
            {
                Main.xdoc.Element("InventoryItems")
                    .Element("Inventory")
                    .SetAttributeValue("EndDate", DateTime.Now.ToString(_dateFormat));
                Close();
            }
        }

        private void SetStartDate()
        {
            if (!_started)
            {
                XElement inv = new XElement("Inventory");
                inv.SetAttributeValue("StartDate", DateTime.Now.ToString(_dateFormat));
                Main.xdoc.Element("InventoryItems").Add(inv);
                _started = true;
                log.Debug("XDoc: {0}", Main.xdoc);
            }
        }

        private TwContainer DecodeBarcode(TwContainer container)
        {
            try
            {
                log.Debug("DecodeBarcode(Container.Id = {0}) --->", container);
                string[] split = tbBarcode.Text.Split(Configuration.Barcode.BarcodeSeparator.ToCharArray());

                if (split.Length >= 3)
                {
                    if (container == null)
                        container = new TwContainer();

                    container.LotCode = split[0];
                    container.Identifier = long.Parse(split[1]);
                    Location l = new Location();
                    string locdata = split[2].Trim();
                    l.Row = locdata.Substring(0, 1);
                    l.Column = locdata.Substring(1, 2);
                    l.Depth = locdata.Substring(3, 2);
                    l.Name = "";
                    l.Position = "";
                    l.Warehouse = new Warehouse();
                    container.Location = l;
                    l = null;
                    locdata = "";

                    return container;
                }
            }
            catch (Exception e)
            {
                log.Error("Could not decode information: {0}", e);
            }

            return null;
        }

        private bool StoreData()
        {
            log.Debug("StoreData() --->");

            try
            {
                log.Debug("XDoc: {0}", Main.xdoc);
                if (_inventory != null)
                {
                    Main.xdoc.Element("InventoryItems").Element("Inventory").Add(_inventory.ToXml());
                    btnContinue.Enabled = btnInventoryEnd.Enabled = _started;
                    tbBarcode.Text = "";
                    tbProductName.Text = "";
                    tbItemCount.Text = "";
                    tbMtrsPerRoll.Text = "";
                    MessageBox.Show("La Información fue guardada correctamente. Ahora puede Finalizar el Inventario o Continuar.", "Cambios Guardados");
                    return true;
                }
                else
                {
                    log.Error("Inventory object is null!");
                }
            }
            catch (Exception e)
            {
                log.Error("An error ocurred trying to save data to disk: {0}", e);
                MessageBox.Show("No se pudo guardar la información ingresada. Revise el registro de errores para mayor información.", "Error");
                btnInventoryEnd.Enabled = false;
                btnContinue.Enabled = true;
            }

            return false;
        }

        #region Event Handlers

        private void btnSquare_Click(object sender, EventArgs e)
        {
            OnBtnSquareClick();
        }

        private void btnDiff_Click(object sender, EventArgs e)
        {
            OnBtnDiffClick();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            OnContinue();
        }

        private void btnInventoryEnd_Click(object sender, EventArgs e)
        {
            OnBtnInventoryEndClick();
        }

        #endregion Event Handlers
    }
}