﻿namespace InventoryCE.View
{
    partial class Main
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewProduct = new System.Windows.Forms.Button();
            this.btnMovePallet = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnInventory = new System.Windows.Forms.Button();
            this.mainMenu = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.fileExit = new System.Windows.Forms.MenuItem();
            this.scannerMenu = new System.Windows.Forms.MenuItem();
            this.scannerOn = new System.Windows.Forms.MenuItem();
            this.scannerOff = new System.Windows.Forms.MenuItem();
            this.btnExtractProduct = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNewProduct
            // 
            this.btnNewProduct.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnNewProduct.Enabled = false;
            this.btnNewProduct.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnNewProduct.Location = new System.Drawing.Point(3, 60);
            this.btnNewProduct.Name = "btnNewProduct";
            this.btnNewProduct.Size = new System.Drawing.Size(110, 62);
            this.btnNewProduct.TabIndex = 0;
            this.btnNewProduct.Text = "Ingresar Lote";
            this.btnNewProduct.Click += new System.EventHandler(this.btnNewProduct_Click);
            // 
            // btnMovePallet
            // 
            this.btnMovePallet.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMovePallet.Enabled = false;
            this.btnMovePallet.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnMovePallet.Location = new System.Drawing.Point(125, 60);
            this.btnMovePallet.Name = "btnMovePallet";
            this.btnMovePallet.Size = new System.Drawing.Size(110, 62);
            this.btnMovePallet.TabIndex = 1;
            this.btnMovePallet.Text = "Mover Pallet";
            this.btnMovePallet.Click += new System.EventHandler(this.btnMovePallet_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(28, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 20);
            this.label1.Text = "Haga clic sobre una acción:";
            // 
            // btnInventory
            // 
            this.btnInventory.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnInventory.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnInventory.Location = new System.Drawing.Point(125, 128);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(110, 62);
            this.btnInventory.TabIndex = 3;
            this.btnInventory.Text = "Inventariar";
            this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.Add(this.menuItem1);
            this.mainMenu.MenuItems.Add(this.scannerMenu);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.fileExit);
            this.menuItem1.Text = "Archivo";
            // 
            // fileExit
            // 
            this.fileExit.Text = "Salir";
            this.fileExit.Click += new System.EventHandler(this.fileExit_Click);
            // 
            // scannerMenu
            // 
            this.scannerMenu.Enabled = false;
            this.scannerMenu.MenuItems.Add(this.scannerOn);
            this.scannerMenu.MenuItems.Add(this.scannerOff);
            this.scannerMenu.Text = "Capturador";
            // 
            // scannerOn
            // 
            this.scannerOn.Enabled = false;
            this.scannerOn.Text = "Encender";
            this.scannerOn.Click += new System.EventHandler(this.scannerOn_Click);
            // 
            // scannerOff
            // 
            this.scannerOff.Enabled = false;
            this.scannerOff.Text = "Apagar";
            this.scannerOff.Click += new System.EventHandler(this.scannerOff_Click);
            // 
            // btnExtractProduct
            // 
            this.btnExtractProduct.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnExtractProduct.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnExtractProduct.Location = new System.Drawing.Point(3, 128);
            this.btnExtractProduct.Name = "btnExtractProduct";
            this.btnExtractProduct.Size = new System.Drawing.Size(110, 62);
            this.btnExtractProduct.TabIndex = 5;
            this.btnExtractProduct.Text = "Extraer Rollos";
            this.btnExtractProduct.Click += new System.EventHandler(this.btnExtractProduct_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 263);
            this.Controls.Add(this.btnExtractProduct);
            this.Controls.Add(this.btnInventory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMovePallet);
            this.Controls.Add(this.btnNewProduct);
            this.MaximizeBox = false;
            this.Menu = this.mainMenu;
            this.Name = "Main";
            this.Text = "Control de Inventario";
            this.Load += new System.EventHandler(this.Main_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Main_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNewProduct;
        private System.Windows.Forms.Button btnMovePallet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInventory;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem fileExit;
        private System.Windows.Forms.MenuItem scannerMenu;
        private System.Windows.Forms.MenuItem scannerOn;
        private System.Windows.Forms.MenuItem scannerOff;
        private System.Windows.Forms.Button btnExtractProduct;


    }
}