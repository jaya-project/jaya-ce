﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using InventoryCE.Controller;
using InventoryCE.Model;
using InventoryCE.Model.DbModel;
using InventoryLibraries.SQLite;
using InventoryLibraries.Util;
using NLog;

namespace InventoryCE.View
{
    public partial class MovePallet : Form
    {
        private Logger log = LogManager.GetLogger("MovePallet");

        private SQLiteWrapper _wrap = SQLiteWrapper.GetInstance(Configuration.DBFile);
        private TwContainer _container = new TwContainer();
        private Location _sourceLoc = null;

        public MovePallet(string bcdata)
        {
            InitializeComponent();
            tbBcdata.Text = bcdata;
            FillRowComboBox();
            ReadData();
            SetInitialData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            StoreData();
        }

        private void StoreData()
        {
            btnSave.Enabled = false;

            if (ValidateData())
            {
                log.Debug("StoreData() --->");

                try
                {
                    string tmp = "";
                    _container.Location = new Location();

                    if (nColumn.Value < 10)
                        tmp = "0";

                    _container.Location.Column = string.Format("{0}{1}", tmp, nColumn.Value.ToString());
                    tmp = "";

                    if (nDepth.Value < 10)
                        tmp = "0";

                    _container.Location.Depth = string.Format("{0}{1}", tmp, nDepth.Value.ToString());
                    _container.Location.Row = cbRow.SelectedItem.ToString();

                    UserContainer uc = new UserContainer();
                    uc.Container = _container;
                    uc.ContainerId = _container.Id;
                    uc.Action = UserContainer.ActionName.Move;
                    uc.ActionDate = DateTime.Now;
                    uc.User = SessionController.CurrentUser;
                    uc.UserId = uc.User.Id;

                    XElement element = new XElement("PalletMove",
                            _container.ToXml(),
                            new XElement("SourceLocation", _sourceLoc.ToXml()),
                            new XElement("DestLocation", _container.Location.ToXml()));
                    log.Debug(element.ToString());
                    RemoveExistingContainer(_container.Identifier, _container.LotCode);
                    Main.xdoc.Element("PalletMoves").Add(element);
                    MessageBox.Show("Los cambios fueron guardaos. Ahora la ventana se cerrará.", "Cambios Guardados");
                    Close();
                }
                catch (Exception e)
                {
                    log.Error("An error ocurred trying to save data to disk: {0}", e);

                    if (!this.IsDisposed)
                        btnSave.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Hay errores en los valores ingresados. Revise la información ingresada y vuelva a intentarlo.");
                btnSave.Enabled = true;
            }
        }

        private bool ValidateData()
        {
            log.Debug("ValidateData() --->");
            bool r = true;

            if (cbRow.SelectedIndex == 0)
            {
                Utils.SetInvalidControl(cbRow);
                r = false;
            }
            else
            {
                Utils.SetValidControl(cbRow);
            }

            Utils.SetValidControl(nColumn);
            Utils.SetValidControl(nDepth);

            return r;
        }

        #region Inital Setup

        private void FillRowComboBox()
        {
            try
            {
                UIUtils.FillRowCombo(cbRow);
            }
            catch (Exception e)
            {
                log.Error("Could not fill rack data: {0}", e);
            }
        }

        private void SetInitialData()
        {
            if (_container != null)
            {
                log.Debug("SetInitialData() --->");
                tbProductName.Text = _container.Item.Name;
                SetComboBoxRowIndex(_container.Location.Row);
                nColumn.Value = Convert.ToDecimal(_container.Location.Column);
                nDepth.Value = Convert.ToDecimal(_container.Location.Depth);
            }
        }

        private void ReadData()
        {
            DataTable dt = null;

            try
            {
                log.Debug("ReadData() --->");
                _container = DecodeBarcode(_container);

                if (_container != null)
                {
                    SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                    where.Append("Container.identifier = {0} AND Container.lotCode = {1}", new object[] { _container.Identifier, _container.LotCode });
                    StringBuilder expr = new StringBuilder();
                    expr.Append("Item.itemId AS iid, Item.itemName AS iname, Item.itemCode AS icode, ");
                    expr.Append("Location.locationId AS lid, Location.row, Location.column, Location.depth, Location.warehouseId, ");
                    expr.Append("Container.containerId AS cid, itemCount, itemTotalLength, perItemAvgLength, unitPrice");
                    string columns = expr.ToString();
                    expr = new StringBuilder();
                    expr.AppendFormat("INNER JOIN {0} ON Item.itemId = Container.itemId ", "Item");
                    expr.AppendFormat("INNER JOIN {0} ON Location.locationId = Container.locationId", "Location");
                    string join = expr.ToString();
                    dt = _wrap.Query(
                        "Container",
                        columns,
                        join,
                        where,
                        1,
                        0);
                    expr = null;
                    columns = "";
                    join = "";
                    where = null;

                    if (dt != null && dt.Rows.Count == 1)
                    {
                        DataRow r = dt.Rows[0];
                        _container.Id = StringUtils.GetLong(r["cid"]);
                        _container.ItemCount = StringUtils.GetInt(r["itemCount"]);
                        _container.ItemTotalLength = Convert.ToDouble(StringUtils.GetInt(r["itemTotalLength"]));
                        _container.UnitPrice = Convert.ToDouble(StringUtils.GetInt(r["unitPrice"]));
                        _container.Item = new InventoryCE.Model.DbModel.Item();
                        _container.Item.Id = StringUtils.GetLong(r["iid"]);
                        _container.Item.Code = StringUtils.GetString(r["icode"]);
                        _container.Item.Name = StringUtils.GetString(r["iname"]);
                        _container.Location = new Location();
                        _container.Location.Id = StringUtils.GetLong(r["lid"]);
                        _container.Location.Row = StringUtils.GetString(r["row"]);
                        _container.Location.Column = StringUtils.GetString(r["column"]);
                        _container.Location.Depth = r["depth"].ToString();
                        _container.Location.Warehouse = new Warehouse();
                        _container.Location.Warehouse.Id = StringUtils.GetLong(r["warehouseId"]);
                        _sourceLoc = _container.Location;
                        _sourceLoc.Id = 0;
                        r = null;
                    }
                    else
                    {
                        _container = null;
                    }
                }
                else
                {
                    MessageBox.Show("No se pudo decodificar el código de barras. Revise el registro de errores.");
                    Close();
                }
            }
            catch (Exception e)
            {
                log.Error("Error reading container information: {0}", e);
                Close();
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }
        }

        private void RemoveExistingContainer(long identifier, string lotcode)
        {
            try
            {
                if (Main.xdoc.Element("PalletMoves").HasElements)
                {
                    List<XElement> list = (from node in Main.xdoc.Element("PalletMoves").Elements("PalletMove")
                                           where long.Parse(node.Element("Container").Element("Identifier").Value) == identifier &&
                                                 node.Element("Container").Element("LotCode").Value == lotcode
                                           select node).ToList();

                    if (list != null && list.Count == 1)
                    {
                        list.First().Remove();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Failed to remove existing pallet move: {0}", ex);
            }
        }

        private void SetComboBoxRowIndex(string row)
        {
            if (cbRow.Items.Count > 0)
            {
                log.Debug("SetComboBoxRowIndex(row = {0}) --->", row);
                for (int i = 0; i < cbRow.Items.Count; i++)
                {
                    string currentRow = cbRow.Items[i].ToString();
                    log.Debug("CurrentRow: {0}", currentRow);

                    if (currentRow.ToLower() == row.ToLower())
                    {
                        cbRow.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        private TwContainer DecodeBarcode(TwContainer container)
        {
            try
            {
                log.Debug("DecodeBarcode(Container.Id = {0}) --->", container.Id);
                string[] split = tbBcdata.Text.Split(Configuration.Barcode.BarcodeSeparator.ToCharArray());

                if (split.Length >= 3)
                {
                    container.LotCode = split[0];
                    container.Identifier = long.Parse(split[1]);
                    return container;
                }
            }
            catch (Exception e)
            {
                log.Error("Failed decode bar code information: {0}", e);
            }

            return null;
        }

        private Location GetLocation(string row, string column, string depth)
        {
            DataTable dt = null;
            Location l = null;
            SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
            where.Append("row = {0} AND column = {1} AND depth = {2}", new object[] {
                row, column, depth
            });
            string join = "LEFT OUTER JOIN Warehouse ON Warehouse.warehouseId = Location.warehouseId ";
            join += "LEFT OUTER JOIN WarehouseCode ON WarehouseCode.warehouseCodeId = Warehouse.warehouseCodeId";

            try
            {
                dt = _wrap.Query("Location",
                    "Location.*, Warehouse.warehouseCodeId,whName,whSize,whCode,description", where);

                if (dt != null && dt.Rows.Count >= 1)
                {
                    DataRow r = dt.Rows[0];
                    l = new Location();
                    l.Name = StringUtils.GetString(r["name"]);
                    l.Position = StringUtils.GetString(r["position"]);
                    l.Row = StringUtils.GetString(r["row"]).ToString();
                    l.Column = StringUtils.GetString(r["column"]);
                    l.Depth = StringUtils.GetString(r["depth"]);
                    l.Id = StringUtils.GetLong(r["locationId"]);

                    WarehouseCode wc = new WarehouseCode();
                    wc.Code = StringUtils.GetString(r["whCode"]);
                    wc.Description = StringUtils.GetString(r["description"]);
                    wc.Id = StringUtils.GetLong(r["warehouseCodeId"]);

                    Warehouse w = new Warehouse();
                    w.Id = StringUtils.GetLong(r["warehouseId"]);
                    w.Name = StringUtils.GetString(r["whName"]);
                    w.Size = StringUtils.GetString(r["whSize"]);
                    w.WarehouseCode = wc;
                    wc = null;

                    l.Warehouse = w;
                    w = null;
                }
                else
                {
                    log.Debug("DataTable was null or there were no results!");
                }

                dt = null;
            }
            catch (Exception e)
            {
                log.Error("Failed to retrieve location '{0}{1}{2}' information: {3}", e);
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }

            return l;
        }

        #endregion Initial Setup
    }
}