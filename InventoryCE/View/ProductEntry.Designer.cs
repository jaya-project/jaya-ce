﻿namespace InventoryCE.View
{
    partial class ProductEntry
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductEntry));
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.pSearchControls = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.PictureBox();
            this.pProductInfo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbProductName = new System.Windows.Forms.TextBox();
            this.tbItemCode = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pLotCode = new System.Windows.Forms.Panel();
            this.btnLotOk = new System.Windows.Forms.Button();
            this.btnScanCode = new System.Windows.Forms.PictureBox();
            this.tbLotCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pAddPallets = new System.Windows.Forms.Panel();
            this.btnAddPallets = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.pWarehouse = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.cbWarehouse = new System.Windows.Forms.ComboBox();
            this.pSearchControls.SuspendLayout();
            this.pProductInfo.SuspendLayout();
            this.pLotCode.SuspendLayout();
            this.pAddPallets.SuspendLayout();
            this.pWarehouse.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbSearch
            // 
            this.tbSearch.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tbSearch.Location = new System.Drawing.Point(6, 24);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(164, 23);
            this.tbSearch.TabIndex = 0;
            this.tbSearch.GotFocus += new System.EventHandler(this.TextBox_FocusIn);
            this.tbSearch.LostFocus += new System.EventHandler(this.TextBox_FocusOut);
            // 
            // lblSearch
            // 
            this.lblSearch.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSearch.Location = new System.Drawing.Point(3, 2);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(197, 19);
            this.lblSearch.Text = "2.- Busque el Producto";
            // 
            // pSearchControls
            // 
            this.pSearchControls.Controls.Add(this.btnSearch);
            this.pSearchControls.Controls.Add(this.tbSearch);
            this.pSearchControls.Controls.Add(this.lblSearch);
            this.pSearchControls.Enabled = false;
            this.pSearchControls.Location = new System.Drawing.Point(3, 66);
            this.pSearchControls.Name = "pSearchControls";
            this.pSearchControls.Size = new System.Drawing.Size(207, 50);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(176, 23);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(24, 24);
            this.btnSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // pProductInfo
            // 
            this.pProductInfo.Controls.Add(this.label1);
            this.pProductInfo.Controls.Add(this.label11);
            this.pProductInfo.Controls.Add(this.tbProductName);
            this.pProductInfo.Controls.Add(this.tbItemCode);
            this.pProductInfo.Controls.Add(this.label12);
            this.pProductInfo.Location = new System.Drawing.Point(3, 122);
            this.pProductInfo.Name = "pProductInfo";
            this.pProductInfo.Size = new System.Drawing.Size(207, 80);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(6, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 20);
            this.label1.Text = "3.- Revise la Información";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(7, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 20);
            this.label11.Text = "Código:";
            // 
            // tbProductName
            // 
            this.tbProductName.Location = new System.Drawing.Point(74, 54);
            this.tbProductName.Name = "tbProductName";
            this.tbProductName.ReadOnly = true;
            this.tbProductName.Size = new System.Drawing.Size(127, 23);
            this.tbProductName.TabIndex = 10;
            // 
            // tbItemCode
            // 
            this.tbItemCode.Location = new System.Drawing.Point(74, 25);
            this.tbItemCode.Name = "tbItemCode";
            this.tbItemCode.ReadOnly = true;
            this.tbItemCode.Size = new System.Drawing.Size(127, 23);
            this.tbItemCode.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(7, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 20);
            this.label12.Text = "Nombre:";
            // 
            // pLotCode
            // 
            this.pLotCode.Controls.Add(this.btnLotOk);
            this.pLotCode.Controls.Add(this.btnScanCode);
            this.pLotCode.Controls.Add(this.tbLotCode);
            this.pLotCode.Controls.Add(this.label2);
            this.pLotCode.Location = new System.Drawing.Point(3, 3);
            this.pLotCode.Name = "pLotCode";
            this.pLotCode.Size = new System.Drawing.Size(207, 57);
            // 
            // btnLotOk
            // 
            this.btnLotOk.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnLotOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLotOk.Location = new System.Drawing.Point(163, 25);
            this.btnLotOk.Name = "btnLotOk";
            this.btnLotOk.Size = new System.Drawing.Size(38, 24);
            this.btnLotOk.TabIndex = 3;
            this.btnLotOk.Text = "Listo";
            this.btnLotOk.Click += new System.EventHandler(this.btnLotOk_Click);
            // 
            // btnScanCode
            // 
            this.btnScanCode.Image = ((System.Drawing.Image)(resources.GetObject("btnScanCode.Image")));
            this.btnScanCode.Location = new System.Drawing.Point(133, 24);
            this.btnScanCode.Name = "btnScanCode";
            this.btnScanCode.Size = new System.Drawing.Size(24, 24);
            this.btnScanCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnScanCode.Click += new System.EventHandler(this.btnScanCode_Click);
            // 
            // tbLotCode
            // 
            this.tbLotCode.Location = new System.Drawing.Point(6, 25);
            this.tbLotCode.Name = "tbLotCode";
            this.tbLotCode.Size = new System.Drawing.Size(121, 23);
            this.tbLotCode.TabIndex = 1;
            this.tbLotCode.TextChanged += new System.EventHandler(this.tbLotCode_TextChanged);
            this.tbLotCode.GotFocus += new System.EventHandler(this.TextBox_FocusIn);
            this.tbLotCode.LostFocus += new System.EventHandler(this.TextBox_FocusOut);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 20);
            this.label2.Text = "1.- Ingrese No. de Lote";
            // 
            // pAddPallets
            // 
            this.pAddPallets.Controls.Add(this.btnAddPallets);
            this.pAddPallets.Controls.Add(this.label3);
            this.pAddPallets.Enabled = false;
            this.pAddPallets.Location = new System.Drawing.Point(3, 288);
            this.pAddPallets.Name = "pAddPallets";
            this.pAddPallets.Size = new System.Drawing.Size(207, 80);
            // 
            // btnAddPallets
            // 
            this.btnAddPallets.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnAddPallets.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAddPallets.Location = new System.Drawing.Point(6, 41);
            this.btnAddPallets.Name = "btnAddPallets";
            this.btnAddPallets.Size = new System.Drawing.Size(194, 36);
            this.btnAddPallets.TabIndex = 5;
            this.btnAddPallets.Text = "Añadir Pallets";
            this.btnAddPallets.Click += new System.EventHandler(this.btnAddPallets_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(6, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(194, 35);
            this.label3.Text = "5.- Añade los pallets del Lote";
            // 
            // pWarehouse
            // 
            this.pWarehouse.Controls.Add(this.label4);
            this.pWarehouse.Controls.Add(this.cbWarehouse);
            this.pWarehouse.Enabled = false;
            this.pWarehouse.Location = new System.Drawing.Point(3, 208);
            this.pWarehouse.Name = "pWarehouse";
            this.pWarehouse.Size = new System.Drawing.Size(207, 74);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(6, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(194, 40);
            this.label4.Text = "4.- Seleccione una Bodega";
            // 
            // cbWarehouse
            // 
            this.cbWarehouse.Location = new System.Drawing.Point(6, 45);
            this.cbWarehouse.Name = "cbWarehouse";
            this.cbWarehouse.Size = new System.Drawing.Size(197, 23);
            this.cbWarehouse.TabIndex = 70;
            this.cbWarehouse.SelectedIndexChanged += new System.EventHandler(this.cbWarehouse_SelectedIndexChanged);
            // 
            // ProductEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(241, 275);
            this.Controls.Add(this.pWarehouse);
            this.Controls.Add(this.pAddPallets);
            this.Controls.Add(this.pLotCode);
            this.Controls.Add(this.pProductInfo);
            this.Controls.Add(this.pSearchControls);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductEntry";
            this.Text = "Ingreso de Lote";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ProductEntry_Closing);
            this.pSearchControls.ResumeLayout(false);
            this.pProductInfo.ResumeLayout(false);
            this.pLotCode.ResumeLayout(false);
            this.pAddPallets.ResumeLayout(false);
            this.pWarehouse.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Panel pSearchControls;
        private System.Windows.Forms.Panel pProductInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbProductName;
        private System.Windows.Forms.TextBox tbItemCode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel pLotCode;
        private System.Windows.Forms.TextBox tbLotCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox btnScanCode;
        private System.Windows.Forms.PictureBox btnSearch;
        private System.Windows.Forms.Panel pAddPallets;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddPallets;
        private System.Windows.Forms.Panel pWarehouse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbWarehouse;
        private System.Windows.Forms.Button btnLotOk;
    }
}