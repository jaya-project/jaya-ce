﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using InventoryCE.Model;
using NLog;
using SYSCTL;

namespace InventoryCE.View
{
    public partial class BCScan : Form
    {
        /// <summary>
        /// Singleton pattern instance
        /// </summary>
        private BarcodeReader _bcr;
        private Logger log = LogManager.GetLogger("BCScan");
        private string _barcodeData;
        private Regex _validator = null;

        /// <summary>
        /// Returns the barcode data (if any).
        /// </summary>
        public string Barcode
        {
            get
            {
                return _barcodeData;
            }
        }

        public BCScan(Regex validator)
        {
            InitializeComponent();

            if (validator != null)
                _validator = validator;

            btnHash.Text += Configuration.Barcode.BarcodeSeparator;
            _bcr = new BarcodeReader();
            _bcr.NotifyEventHandler += new BarcodeReader.BCRNotifyEventHandler(bcr_NotifyEventHandler);
            _bcr.TerminalChar = TermCharType.NONE;
            _bcr.OutputMode = OutputModeType.DISABLE;
        }

        public BCScan()
            : this(null) { }

        private void bcr_NotifyEventHandler(object sender, NotifyEventArgs e)
        {
            OnBcrRead(e);
        }

        private void BCScan_Resize(object sender, EventArgs e)
        {
            this.Width = 240;
            this.Height = 300;
        }

        private void BCScan_Closing(object sender, CancelEventArgs e)
        {
            OnClose();
        }

        private void OnClose()
        {
            log.Debug("Closing!");
            _bcr.NotifyEventHandler -= bcr_NotifyEventHandler;
            _bcr = null;
        }

        private void OnBcrRead(NotifyEventArgs e)
        {
            try
            {

                if (e.NotifyEvent == NotifyEventType.RECEIVE_BARCODE)
                {
                    if (e.BarcodeData.Length > 0)
                    {
                        _barcodeData = Regex.Replace(e.BarcodeData,
                                   Configuration.Barcode.RemoveBarcodeChars, "",
                                   RegexOptions.Multiline | RegexOptions.Compiled).Trim();

                        if (Validate(_barcodeData))
                        {
                            log.Debug("Received data: {0}", _barcodeData);
                            //Clipboard.SetDataObject(_barcodeData);
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("El código de barras escaneado no es válido. Puede que se haya equivocado de etiqueta. Si el error persiste, ingrese la información manualmente.", "Código no válido");
                            _barcodeData = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("An error ocurred processing barcode read: {0}", ex);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (manualInput.Text.Length > 0)
            {
                if (Validate(manualInput.Text))
                {
                    _barcodeData = manualInput.Text;
                    log.Debug("Input barcode: {0}", _barcodeData);
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("El código ingresado no es válido. Asegúrese de estar leyendo la etiqueta correcta.", "Código no válido");
                }
            }
        }

        private void manualInput_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (tb.Text.Length > 0)
            {
                btnOk.Enabled = true;
            }
            else
            {
                btnOk.Enabled = false;
            }
        }

        /// <summary>
        /// Validates the data using the Regex expression passed with the constructor.
        /// </summary>
        /// <param name="input">The data read with barcode scanner or manually input.</param>
        /// <returns>True if the input is correct, false otherwise.</returns>
        private bool Validate(string input)
        {
            if (_validator == null)
                return true;

            log.Debug("Using the following regex to validate the bar code: {0}", _validator.ToString());

            return _validator.IsMatch(input);
        }

        private void CheckBCRPower()
        {
            if (!_bcr.EnablePower)
            {
                _bcr.EnablePower = true;
            }
        }

        private void btnHash_Click(object sender, EventArgs e)
        {
            manualInput.Capture = true;
            manualInput.Text = manualInput.Text.Insert(manualInput.SelectionStart, Configuration.Barcode.BarcodeSeparator);
            manualInput.Focus();
            manualInput.SelectionStart = manualInput.Text.Length;
        }
    }
}