﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using InventoryCE.Model;
using InventoryCE.Model.DbModel;
using InventoryLibraries.SQLite;
using InventoryLibraries.Util;
using NLog;
using System.Collections;

namespace InventoryCE.View
{
    public partial class ProductEntry : Form
    {

        #region Members
        private Logger log = LogManager.GetLogger("ProductEntry");
        private readonly String[] _defaults = { "Número de Lote", "Nombre o Código Producto" };
        private string _lastSearch = "";
        private XElement _lot = null;

        // Model
        private SQLiteWrapper _wrap = SQLiteWrapper.GetInstance(Configuration.DBFile);
        private TwContainer _container;
        private List<Provider> _providerList = new List<Provider>();
        private List<Warehouse> _warehouseList = new List<Warehouse>();
        private string _where = "";
        private string _join = "";

        // Variable to know if the data should be stored to the XML object.
        private bool _storeData = false;

        private int _palletId = 1;

        private Object lockParser = new Object();

        #endregion Members

        public ProductEntry()
        {
            InitializeComponent();
            _container = new TwContainer();
            tbLotCode.Text = _defaults[0];
            tbSearch.Text = _defaults[1];
            this.Width = Program.Width;
            this.Height = Program.Height;
            tbLotCode.Focus();
        }

        #region Event Handlers

        private void TextBox_FocusIn(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            String txt = _defaults[GetTextBoxIndex(tb)];

            if (tb.Text.Equals(txt, StringComparison.InvariantCultureIgnoreCase))
            {
                tb.Text = "";
            }
        }

        private void TextBox_FocusOut(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (tb.Text.Length <= 0)
            {
                tb.Text = _defaults[GetTextBoxIndex(tb)];
            }
        }

        private void ProductEntry_Closing(object sender, CancelEventArgs e)
        {
            OnClosing();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            OnSearch(tbSearch.Text);
            btnSearch.Enabled = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void btnScanCode_Click(object sender, EventArgs e)
        {
            OnScanCode();
        }

        private void btnAddPallets_Click(object sender, EventArgs e)
        {
            OnAddPallet();
        }

        private void btnLotOk_Click(object sender, EventArgs e)
        {
            OnLotOkClick();
        }

        private void tbLotCode_TextChanged(object sender, EventArgs e)
        {
            btnLotOk.Enabled = tbLotCode.Text != _defaults[GetTextBoxIndex(tbLotCode)] && tbLotCode.Text.Length > 0;
        }

        private void cbWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWarehouse.SelectedIndex > 0)
            {
                pAddPallets.Enabled = true;
                btnAddPallets.Focus();
            }
            else
            {
                pAddPallets.Enabled = false;
            }
        }

        #endregion Event Handlers

        #region Event Processing (On... Functions)

        /// <summary>
        /// Handles the Closing event.
        /// </summary>
        private void OnClosing()
        {
            log.Debug("OnClosing() --->");
            StoreData();
            _lastSearch = null;
            _lot = null;
            _wrap = null;
            _container = null;
            log = null;
            _where = null;
            _join = null;
            _providerList = null;

            if (_warehouseList != null && _warehouseList.Any())
            {
                _warehouseList.Clear();
                _warehouseList = null;
            }
        }

        /// <summary>
        /// Searches for the product based on the input. If it's a number, then it searches for the specific product with that code,
        /// whereas if it's a string, it will search for all the products that could match that string.
        /// </summary>
        /// <param name="search">The product to search for.</param>
        private void OnSearch(string search)
        {
            search = search.Trim();
            
            if (search.Length > 0)
            {
                this.Enabled = false;
                DataTable dt = null;

                try
                {
                    CreateConditions(search);
                    StringBuilder columns = new StringBuilder();
                    columns.Append("itemName AS Producto,");
                    columns.Append("itemCode AS `Código Producto`");
                    SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                    where.Append(_where, search);
                    dt = _wrap.Query("Item",
                        columns.ToString(),
                        _join,
                        where);

                    where = null;
                    columns = null;
                    _container.Item = null;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        Utils.LogColumns(log, dt);
                        if (dt.Rows.Count > 1)
                        {
                            ItemSelection itemSelectionForm = new ItemSelection(dt);
                            this.DialogResult = itemSelectionForm.ShowDialog();

                            if (DialogResult == DialogResult.OK)
                            {
                                _container.Item = new Item();
                                _container.Item.Name = itemSelectionForm.ItemName;
                                _container.Item.Code = itemSelectionForm.ItemCode;
                            }
                            else
                            {
                                log.Error("OnSearch - Could not select the right item.");
                            }
                        }
                        else
                        {
                            DataRow row = dt.Rows[0];
                            _container.Item = new Item();
                            _container.Item.Name = row["Producto"].ToString();
                            _container.Item.Code = row["Código Producto"].ToString();
                            row = null;
                        }

                        if (_container.Item != null)
                        {
                            _container.Item = FillItemMeta(_container.Item);
                            _container.Item.Properties = GetItemProperties(_container.Item);
                            tbItemCode.Text = _container.Item.Code;
                            tbProductName.Text = _container.Item.Name;
                            FillWarehouseCombo(0);
                            pWarehouse.Enabled = true;
                            cbWarehouse.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("No se encontró el producto buscado.");
                        ChangeControlState(
                            new Control[] { pAddPallets }, false);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Ocurrió un error al leer la información del producto: " + e.Message);
                    log.Error("An error ocurred reading product info: {0} - {1}", e.Message, e);
                }
                finally
                {
                    if (dt != null)
                        dt.Dispose();

                    dt = null;
                }

                this.Enabled = true;
            }
            else
            {
                log.Info("The search string ({0}) is empty or is the same as the last ({1}).", search, _lastSearch);
            }
        }

        /// <summary>
        /// Handles the LotCode textbox text change.
        /// </summary>
        private void OnLotOkClick()
        {
            tbProductName.Text = tbItemCode.Text = tbSearch.Text = "";

            if (Main.xdoc.Element("Lots").HasElements)
            {
                log.Info("Searching lot code '{0}' in XML data...", tbLotCode.Text);
                XElement lot = FindLotInXML(tbLotCode.Text);

                if (lot != null)
                {
                    log.Info("Lot '{0}' Found.", tbLotCode.Text);

                    try
                    {
                        // Fill Data and disable controls.
                        _container.Item = GetItemFromXElement(lot);
                        _container.Location = new Location();
                        _container.Location.Warehouse = GetWarehouseFromXElement(lot);
                        tbSearch.Text = "";
                        tbItemCode.Text = _container.Item.Code;
                        tbProductName.Text = _container.Item.Name;
                        FillWarehouseCombo(_container.Location.Warehouse.Id);
                        _lot = lot;
                        _storeData = false;
                        btnAddPallets.Focus();
                        pAddPallets.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed to setup initial data: {0}", ex);
                    }
                }
                else
                {
                    log.Info("No lot found.");
                    _lot = null;
                    tbProductName.Text = tbItemCode.Text = "";
                    pSearchControls.Enabled = true;
                    tbSearch.Focus();
                }
            }
            else
            {
                pSearchControls.Enabled = true;
                tbSearch.Focus();
            }
        }

        /// <summary>
        /// Handles the Scan Button click.
        /// </summary>
        private void OnScanCode()
        {
            Regex regex = new Regex(@"^(\(\d+\)\d+){3}$", RegexOptions.Compiled | RegexOptions.Multiline);
            BCScan scan = new BCScan(regex);

            if (scan.ShowDialog() == DialogResult.OK)
            {
                ParseBarcode(scan.Barcode);
                btnLotOk_Click(btnLotOk, EventArgs.Empty);
            }
            else
            {
                MessageBox.Show("El código escaneado no es válido.", "Código no válido");
            }

            if (!scan.IsDisposed)
                scan.Dispose();

            this.Focus();
            scan = null;
        }

        /// <summary>
        /// Process the add pallet form.
        /// </summary>
        private void OnAddPallet()
        {
            ChangeControlState(false);

            if (ValidateInput())
            {
                pWarehouse.Enabled = false;
                SetContainerData();
                AddPalletView view = null;

                try
                {
                    CountPallets();
                    view = new AddPalletView(_container, _lot, _palletId);
                    view.ShowDialog();
                    this.Focus();

                    XElement xmlLocs = view.GetXml;

                    if (xmlLocs != null && xmlLocs.HasElements)
                    {
                        if (_lot != null && _lot.Parent != null)
                        {
                            try
                            {
                                _lot.Remove();
                            }
                            catch (Exception ex)
                            {
                                log.Error("Failed to remove Lot element. It may be because the data has not been saved yet. Error: {0}", ex);
                            }
                        }

                        _lot = new XElement("Lot",
                            _container.Location.Warehouse.ToXml(),
                            _container.Item.ToXml(),
                            xmlLocs);
                        _lot.SetAttributeValue("code", tbLotCode.Text);
                        _storeData = true;
                        StoreData();
                    }
                    else
                    {
                        log.Info("Adding pallet data cancelled by the user.");
                    }

                    xmlLocs = null;
                }
                catch (Exception ex)
                {
                    log.Error("OnAddPallet - Adding pallet data operation failed: {0}", ex);
                }
                finally
                {
                    if (view != null)
                    {
                        if (!view.IsDisposed)
                            view.Dispose();

                        view = null;
                    }
                }
            }
            else
            {
                MessageBox.Show("Hay información incorrecta. Por favor corrija los campos en rojo.", "Falta Información");
            }

            pAddPallets.Enabled = true;
        }

        #endregion Event Processing (On... Functions)

        #region Utility Functions

        /// <summary>
        /// Creates the conditions based on the content of the search string.
        /// </summary>
        /// <param name="s">Search String</param>
        private void CreateConditions(string s)
        {
            _join = "";
            _where = "";
            StringBuilder join = new StringBuilder();

            if (Utils.IsNumber(s))
            {
                _join = "LEFT OUTER JOIN ForeignProvider ON ForeignProvider.foreignProviderId = Item.foreignProviderId";
                _where = "ForeignProvider.code = {0}";
            }
            else if (Regex.IsMatch(s, @"^\d{3}\-\d+", RegexOptions.Compiled | RegexOptions.IgnoreCase))
            {
                _where = "Item.itemCode = {0}";
            }
            else if (Regex.IsMatch(s, @"^[a-záéíóúäëïöü\s0-9\-_]{2,}$", RegexOptions.Compiled | RegexOptions.IgnoreCase))
            {
                _where = "Item.itemName LIKE '%'||{0}||'%'";
            }

            log.Debug("CreateConditions - Join: {0}; Where: {1}", _join, _where);
        }

        /// <summary>
        /// Adds the data to the main XML object.
        /// </summary>
        private void StoreData()
        {
            log.Info("ProductEntry.StoreData. Should data be stored? {0}", _storeData);

            if (_storeData)
            {
                log.Debug("StoreData() --->");

                try
                {
                    log.Debug("ProductEntry._lot: {0}", _lot);
                    Main.xdoc.Element("Lots").Add(_lot);
                    _storeData = false;
                }
                catch (Exception e)
                {
                    log.Error("OnStoreData - Failed to store data: {0}", e);
                }
            }
        }

        /// <summary>
        /// Fills the item meta data, which is not required to work, but may be in the future.
        /// </summary>
        /// <param name="item">The item to add the meta data to.</param>
        /// <returns>Returns the filled item on success or the same item passed on failure. If the item was null, then it will be created empty.</returns>
        private Item FillItemMeta(Item item)
        {
            if (item == null)
                item = new Item();

            try
            {
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("Item.itemCode = {0} AND Item.itemName = {1}", new object[] {
                    item.Code, item.Name
                });
                string join = string.Format("INNER JOIN {0} ON {0}.foreignProviderId = Item.foreignProviderId", "ForeignProvider");
                DataTable dt = _wrap.Query(
                    "Item",
                    "Item.*, ForeignProvider.foreignProviderId AS fgid, ForeignProvider.code AS fgcode, ForeignProvider.name AS fgname",
                    join,
                    where,
                    1,
                    0);
                where = null;
                join = "";


                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    item.Id = long.Parse(row["itemId"].ToString());
                    item.ForeignProvider = new ForeignProvider();
                    item.ForeignProvider.Id = long.Parse(row["fgid"].ToString());
                    item.ForeignProvider.Code = long.Parse(row["fgcode"].ToString());
                    item.ForeignProvider.Name = row["fgname"].ToString();
                    item.Code = row["itemCode"].ToString();
                    item.Name = row["itemName"].ToString();

                    row = null;
                }
                else
                {
                    log.Error("There was no information on the Foreign Provider..");
                }

                dt = null;
            }
            catch (Exception e)
            {
                log.Error("Exception caught on FillItemMeta: {0}", e);
            }

            return item;
        }

        /// <summary>
        /// Fetches the item properties.
        /// </summary>
        /// <param name="item">Item data to retrieve properties.</param>
        /// <returns>Returns the list of properties only if item is not null and item.Id is set and
        /// greater than zero. Otherwise it will return null.</returns>
        private List<Property> GetItemProperties(Item item)
        {
            List<Property> props = null;

            if (item != null && item.Id > 0)
            {
                try
                {
                    props = new List<Property>();
                    string join = string.Format("INNER JOIN {0} ON Property.propertyId = ItemProperties.propertyId",
                        "Property");
                    SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                    where.Append("ItemProperties.itemId = {0}", item.Id);
                    DataTable dt = _wrap.Query(
                        "ItemProperties",
                        "Property.*, propertyValue",
                        join,
                        where);
                    where = null;
                    join = "";

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            Property prop = new Property();
                            prop.Id = long.Parse(row["propertyId"].ToString());
                            prop.Name = row["propertyName"].ToString();
                            prop.Value = row["propertyValue"].ToString();
                            prop.Type = (Property.DataType)Enum.Parse(typeof(Property.DataType), row["dataType"].ToString(), true);
                            props.Add(prop);
                        }
                    }

                    dt = null;
                }
                catch (Exception e)
                {
                    log.Error("Could not read item ({0}) properties: {1}", item.Id, e);
                }
            }
            else
            {
                log.Error("GetItemProperties - Item was null or Item.Id was not set: {0}.", item.Id);
            }

            return props;
        }

        /// <summary>
        /// Parses the data of the scanned code.
        /// </summary>
        /// <param name="data">The data of the code.</param>
        private void ParseBarcode(string data)
        {
            String tmp = "";
            tmp = Regex.Replace(data, Configuration.Barcode.RemoveBarcodeChars, "", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
            log.Debug("ParseBarcode({0}) --->", tmp);
            tbLotCode.Text = "";
            int indexOf = data.IndexOf("(10)");
            log.Debug("IndexOf: {0}", indexOf);
            tmp = tmp.Substring(indexOf + 4);
            log.Debug("First Cut: {0}", tmp);
            indexOf = tmp.IndexOf('(');
            log.Debug("Second Search: {0}", indexOf);
            tmp = tmp.Substring(0, indexOf);
            log.Debug("Parsed Data: {0}", tmp);

            if (Utils.IsNumber(tmp))
            {
                log.Debug("{0} is number.", tmp);

                lock (lockParser)
                {
                    tbLotCode.Text = "";
                    tbLotCode.Text = tmp;
                }
            }
        }

        /// <summary>
        /// Find the specific item based on its ID.
        /// </summary>
        /// <param name="id">The item ID we are searching for.</param>
        /// <returns>Returns null on failure and an Item object on success.</returns>
        private Item GetItemFromId(long id)
        {
            Item item = null;

            try
            {
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("itemId = {0}", id);
                DataTable t = _wrap.Query("Item", "itemCode,itemName,foreignProviderId", "", where, 1, 0);

                if (t != null && t.Rows.Count == 1)
                {
                    DataRow r = t.Rows[0];
                    item = new Item();
                    item.Code = r["itemCode"].ToString();
                    item.Id = id;
                    item.Name = r["itemName"].ToString();
                    r = null;
                }

                where = null;
                t.Dispose();
                t = null;
            }
            catch (Exception ex)
            {
                log.Error("Could not find item by id {0}: {1}", id, ex);
            }

            return item;
        }

        /// <summary>
        /// Returns the index of the string in _defaults values.
        /// </summary>
        /// <param name="control">The control that we are looking for</param>
        /// <returns>Returns the index on the _defaults values array.</returns>
        private int GetTextBoxIndex(Control control)
        {
            int index = 0;

            switch (control.Name)
            {
                case "tbSearch":
                    index = 1;
                    break;
            }

            return index;
        }

        /// <summary>
        /// Retrieves the Item element from the XML data object.
        /// </summary>
        /// <param name="el">XML data object where the Item data is.</param>
        /// <returns>An Item object with data if it exists or null if it doesn't.</returns>
        private Item GetItemFromXElement(XElement el)
        {
            Item item = null;

            if (el != null && el.HasElements)
            {
                XElement itm = el.Element("Item");

                if (itm != null)
                {
                    item = new Item();
                    item.Id = long.Parse(itm.Element("ItemId").Value);
                    item.Code = itm.Element("ItemCode").Value;
                    item.Name = itm.Element("ItemName").Value;
                    item.ForeignProvider = GetForeignProviderFromCode(long.Parse(itm.Element("ImportCode").Value));
                }

                itm = null;
            }

            return item;
        }

        /// <summary>
        /// Retrieves the Warehouse object from the XML data if exists.
        /// </summary>
        /// <param name="el">XML data object.</param>
        /// <returns>Returns the Warehouse object with data if it exists or null if it doesn't.</returns>
        private Warehouse GetWarehouseFromXElement(XElement el)
        {
            Warehouse warehouse = null;

            if (el != null && el.HasElements)
            {
                XElement wh = el.Element("Warehouse");

                if (wh != null)
                {
                    try
                    {
                        warehouse = new Warehouse();
                        warehouse.Id = long.Parse(wh.Element("WarehouseId").Value);
                        warehouse.Name = wh.Element("WhName").Value;
                        warehouse.Size = wh.Element("WhSize").Value;
                        warehouse.WarehouseCode = GetWarehouseCodeFromId(long.Parse(wh.Element("WarehouseCodeId").Value));
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed to retrieve Warehouse element from XML: {0}", ex);
                    }
                }
            }

            return warehouse;
        }

        /// <summary>
        /// Searches the XML file for the lot code.
        /// </summary>
        /// <param name="lotCode">The lot code.</param>
        /// <returns>Returns the XElement if there is a Lot element with the attribute code == lotCode or null if it doesn't.</returns>
        private XElement FindLotInXML(string lotCode)
        {
            XElement lot = null;

            if (lotCode != null && lotCode.Length > 0)
            {
                try
                {
                    IEnumerable<XElement> tmp = (
                        from Lot in Main.xdoc.Element("Lots").Elements("Lot")
                        where Lot.Attribute("code").Value == lotCode
                        select Lot
                    );

                    if (tmp.Any())
                    {
                        lot = tmp.First();
                    }
                    else
                    {
                        log.Info("No lot found with code '{0}'", lotCode);
                        lot = null;
                    }

                    tmp = null;
                }
                catch (Exception ex)
                {
                    log.Error("Failed to retrieve Lot element ('{0}') from XML. Error: {1}", lotCode, ex);
                }
            }
            else
            {
                log.Debug("There are no Lot elements under Lots.");
            }

            return lot;
        }

        /// <summary>
        /// Retrieves the Foreign Provider based on the code.
        /// </summary>
        /// <param name="code">Code of the Foreign Provider.</param>
        /// <returns>A Foreign Provider Object if it exists or null if it doesn't.</returns>
        private ForeignProvider GetForeignProviderFromCode(long code)
        {
            ForeignProvider fp = null;

            if (code > 0)
            {
                DataTable dt = null;

                try
                {
                    SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                    where.Append("code = {0}", code);
                    dt = _wrap.Query("ForeignProvider", "foreignProviderId, name", where);
                    where = null;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        fp = new ForeignProvider();
                        fp.Code = code;
                        fp.Id = long.Parse(row["foreignProviderId"].ToString());
                        fp.Name = row["name"].ToString();
                        row = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Failed to retrieve ForeignProvider data (code: {0}). Error: {1}", code, ex);
                }
                finally
                {
                    if (dt != null)
                        dt.Dispose();

                    dt = null;
                }
            }

            return fp;
        }

        private void ChangeControlState(bool state, params Control[] skip)
        {
            ChangeControlState(new Control[] {
                pProductInfo, pSearchControls, pAddPallets
            }, state, skip);
        }

        /// <summary>
        /// Changes the Enabled property of the controls.
        /// </summary>
        /// <param name="controls">The controls that will be Enabled/Disabled</param>
        /// <param name="state">Pass true if the controls will be enabled. False otherwise.</param>
        /// <param name="skip">Variable control list to skip.</param>
        private void ChangeControlState(Control[] controls, bool state, params Control[] skip)
        {
            log.Debug("Changing panel state to {0}", state);

            if (controls != null && controls.Any())
            {
                foreach (Control c in controls)
                {
                    if (!skip.Contains(c))
                    {
                        c.Enabled = state;
                    }
                }
            }
        }

        private bool ValidateInput()
        {
            bool r = true;

            if (cbWarehouse.SelectedIndex <= 0)
            {
                Utils.SetInvalidControl(cbWarehouse);
                r &= false;
            }
            else
            {
                r &= true;
                Utils.SetValidControl(cbWarehouse);
            }

            return r;
        }

        private void SetContainerData()
        {
            if (_container.Location == null)
                _container.Location = new Location();

            _container.Location.Warehouse = GetSelectedWarehouse();

            if (_container.Item == null)
                _container.Item = new Item();

            _container.LotCode = tbLotCode.Text;
        }

        /// <summary>
        /// Counts the pallets on every location to retrieve the pallet identifier.
        /// </summary>
        private void CountPallets()
        {
            log.Info("Counting pallets.");
            _palletId = 1;

            if (_lot != null && _lot.HasElements)
            {
                XElement xmlLocations = _lot.Element("Locations");

                if (xmlLocations.HasElements)
                {
                    foreach (XElement location in xmlLocations.Elements("Location"))
                    {
                        XElement containers = location.Element("Containers");

                        if (containers.HasElements)
                        {
                            _palletId += containers.Elements("Container").Count();
                        }

                        containers = null;
                    }
                }

                xmlLocations = null;
            }

            log.Debug("Last pallet Id: {0}", _palletId);
        }

        /// <summary>
        /// Fills the Warehouse ComboBox with the Warehouse data.
        /// </summary>
        /// <param name="whId">This is the Warehouse ID of the elements that should be selected.
        /// If the value is 0 or less, then the first element is selected.</param>
        private void FillWarehouseCombo(long whId)
        {
            pWarehouse.Enabled = false;
            log.Debug("Filling warehouse ComboBox.");

            if (cbWarehouse.Items.Count > 0)
            {
                cbWarehouse.Items.Clear();

                if (_warehouseList != null && _warehouseList.Any())
                {
                    _warehouseList.Clear();
                    _warehouseList = new List<Warehouse>();
                }
            }

            DataTable dt = null;

            try
            {
                dt = _wrap.Query("WarehouseView",
                     "(whCode || ' - ' || whName) AS RowValue, whName, whSize, warehouseId, warehouseCodeId, whCode, description",
                     "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    cbWarehouse.Items.Add("Seleccione código");
                    cbWarehouse.SelectedIndex = 0;
                    int i = 1;

                    foreach (DataRow row in dt.Rows)
                    {
                        WarehouseCode whcode = new WarehouseCode();
                        whcode.Id = long.Parse(row["warehouseCodeId"].ToString());
                        whcode.Code = row["whCode"].ToString();
                        whcode.Description = row["description"].ToString();

                        Warehouse wh = new Warehouse();
                        wh.Id = long.Parse(row["warehouseId"].ToString());
                        wh.Name = row["whName"].ToString();
                        wh.Size = row["whSize"].ToString();
                        wh.WarehouseCode = whcode;
                        cbWarehouse.Items.Add(row["RowValue"]);

                        if (whId == wh.Id)
                        {
                            cbWarehouse.SelectedIndex = i;
                        }

                        _warehouseList.Add(wh);
                        whcode = null;
                        wh = null;
                        i++;
                    }
                }
                else
                {
                    log.Error("Could not set the values for Warehouse Code!");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }

            pWarehouse.Enabled = true;
        }

        /// <summary>
        /// Fetches the currently selected warehouse (on the cbWarehouse).
        /// </summary>
        /// <returns>A Warehouse Object. It will never return null, as the warehouse should be selected before calling this.
        /// Call ValidateInput before calling this.</returns>
        private Warehouse GetSelectedWarehouse()
        {
            Warehouse wh = new Warehouse();

            try
            {
                string[] split = cbWarehouse.SelectedItem.ToString().Split('-');

                if (split.Length == 2)
                {
                    string whname = split[1].Trim(),
                        whcode = split[0].Trim();
                    wh = _warehouseList.Single(warehouse => warehouse.Name == whname && warehouse.WarehouseCode.Code == whcode);
                }
                else
                {
                    log.Error("Could not split selected warehouse code: {0} {1}", cbWarehouse.SelectedText, cbWarehouse.SelectedIndex);
                }
            }
            catch (Exception e)
            {
                log.Error("Could not retrieve warehouse information: {0}", e);
            }

            return wh;
        }

        /// <summary>
        /// Retrieves the WarehouseCode object based on the ID.
        /// </summary>
        /// <param name="id">ID of the WarehouseCode object.</param>
        /// <returns>A WarehouseCode object with data if the ID exists. If the ID doesn't exists,
        /// then it will return an empty object.</returns>
        private WarehouseCode GetWarehouseCodeFromId(long id)
        {
            WarehouseCode obj = new WarehouseCode();

            if (id > 0)
            {
                DataTable dt = null;

                try
                {
                    SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                    where.Append("warehouseCodeId = {0}", id);
                    dt = _wrap.Query("WarehouseCode", "", where);
                    where = null;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        obj.Code = row["whCode"].ToString();
                        obj.Description = row["description"].ToString();
                        row = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Failed to retrieve WarehouseCode data (id: {0}). Error: {1}", id, ex);
                }
                finally
                {
                    if (dt != null)
                        dt.Dispose();

                    dt = null;
                }
            }

            return obj;
        }

        #endregion Utility Functions
    }
}