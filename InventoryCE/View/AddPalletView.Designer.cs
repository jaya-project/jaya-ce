﻿namespace InventoryCE.View
{
    partial class AddPalletView
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPalletView));
            this.label21 = new System.Windows.Forms.Label();
            this.nLocCol = new System.Windows.Forms.NumericUpDown();
            this.nLocDepth = new System.Windows.Forms.NumericUpDown();
            this.cbLocRow = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.tlScan = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbTotalLength = new System.Windows.Forms.TextBox();
            this.tbRollLength = new System.Windows.Forms.TextBox();
            this.tbRollCount = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(16, 104);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(123, 19);
            this.label21.Text = "Rollos en el Pallet:";
            // 
            // nLocCol
            // 
            this.nLocCol.Location = new System.Drawing.Point(119, 283);
            this.nLocCol.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nLocCol.Name = "nLocCol";
            this.nLocCol.Size = new System.Drawing.Size(83, 24);
            this.nLocCol.TabIndex = 24;
            // 
            // nLocDepth
            // 
            this.nLocDepth.Location = new System.Drawing.Point(119, 313);
            this.nLocDepth.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nLocDepth.Name = "nLocDepth";
            this.nLocDepth.Size = new System.Drawing.Size(83, 24);
            this.nLocDepth.TabIndex = 23;
            // 
            // cbLocRow
            // 
            this.cbLocRow.Location = new System.Drawing.Point(119, 254);
            this.cbLocRow.Name = "cbLocRow";
            this.cbLocRow.Size = new System.Drawing.Size(83, 23);
            this.cbLocRow.TabIndex = 22;
            this.cbLocRow.SelectedIndexChanged += new System.EventHandler(this.cbLocRow_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(13, 313);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 20);
            this.label20.Text = "Estante:";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(13, 283);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 20);
            this.label19.Text = "Cuerpo:";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(13, 254);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 20);
            this.label18.Text = "Rack:";
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.label17.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label17.Location = new System.Drawing.Point(13, 218);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(163, 20);
            this.label17.Text = "Ubicación en Bodega";
            // 
            // lblText
            // 
            this.lblText.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblText.Location = new System.Drawing.Point(13, 2);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(174, 40);
            this.lblText.Text = "Ingrese la información solicitada sobre el pallet:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 20);
            this.label1.Text = "-----------------------------------------";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 20);
            this.label2.Text = "Metros Totales:";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSave.Location = new System.Drawing.Point(14, 343);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(190, 30);
            this.btnSave.TabIndex = 37;
            this.btnSave.Text = "Guardar";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tlScan
            // 
            this.tlScan.Image = ((System.Drawing.Image)(resources.GetObject("tlScan.Image")));
            this.tlScan.Location = new System.Drawing.Point(172, 73);
            this.tlScan.Name = "tlScan";
            this.tlScan.Size = new System.Drawing.Size(32, 32);
            this.tlScan.Click += new System.EventHandler(this.tlScan_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(188, 20);
            this.label3.Text = "Largo del Rollo en Promedio:";
            // 
            // tbTotalLength
            // 
            this.tbTotalLength.Location = new System.Drawing.Point(13, 78);
            this.tbTotalLength.Name = "tbTotalLength";
            this.tbTotalLength.Size = new System.Drawing.Size(151, 23);
            this.tbTotalLength.TabIndex = 69;
            this.tbTotalLength.TextChanged += new System.EventHandler(this.UpdateRollCount_ValueChanged);
            // 
            // tbRollLength
            // 
            this.tbRollLength.Location = new System.Drawing.Point(13, 175);
            this.tbRollLength.Name = "tbRollLength";
            this.tbRollLength.ReadOnly = true;
            this.tbRollLength.Size = new System.Drawing.Size(151, 23);
            this.tbRollLength.TabIndex = 70;
            // 
            // tbRollCount
            // 
            this.tbRollCount.Location = new System.Drawing.Point(14, 126);
            this.tbRollCount.Name = "tbRollCount";
            this.tbRollCount.Size = new System.Drawing.Size(151, 23);
            this.tbRollCount.TabIndex = 71;
            this.tbRollCount.TextChanged += new System.EventHandler(this.UpdateRollCount_ValueChanged);
            // 
            // AddPalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.tbRollCount);
            this.Controls.Add(this.tbRollLength);
            this.Controls.Add(this.tbTotalLength);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tlScan);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.nLocCol);
            this.Controls.Add(this.nLocDepth);
            this.Controls.Add(this.cbLocRow);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddPalletView";
            this.Text = "Ingreso de Pallets";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PalletData_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown nLocCol;
        private System.Windows.Forms.NumericUpDown nLocDepth;
        private System.Windows.Forms.ComboBox cbLocRow;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.PictureBox tlScan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbTotalLength;
        private System.Windows.Forms.TextBox tbRollLength;
        private System.Windows.Forms.TextBox tbRollCount;
    }
}