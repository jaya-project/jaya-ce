﻿namespace InventoryCE.View
{
    partial class InventoryDifferenceInput
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.numNewRollCount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lblComment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 43);
            this.label1.Text = "Ingrese la Información Solicitada";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 20);
            this.label2.Text = "----------------------------------------------";
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnOk.Location = new System.Drawing.Point(157, 102);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(77, 24);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // numNewRollCount
            // 
            this.numNewRollCount.Location = new System.Drawing.Point(4, 102);
            this.numNewRollCount.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numNewRollCount.Name = "numNewRollCount";
            this.numNewRollCount.Size = new System.Drawing.Size(113, 24);
            this.numNewRollCount.TabIndex = 23;
            this.numNewRollCount.ValueChanged += new System.EventHandler(this.numNewRollCount_ValueChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(230, 34);
            this.label3.Text = "¿Cuántos rollos quedan actualmente en el pallet?";
            // 
            // lblComment
            // 
            this.lblComment.Location = new System.Drawing.Point(3, 134);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(231, 40);
            this.lblComment.Visible = false;
            // 
            // InventoryDifferenceInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 174);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblComment);
            this.Controls.Add(this.numNewRollCount);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "InventoryDifferenceInput";
            this.Text = "Diferencia de Contenido";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.NumericUpDown numNewRollCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblComment;

    }
}