﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using InventoryCE.Model;
using NLog;

namespace InventoryCE.View
{
    class UIUtils
    {
        //private static Logger log = LogManager.GetLogger("UIUtils");

        public static void FillRowCombo(ComboBox combo)
        {
            List<string> list = new List<string>();
            list.Add("Seleccione un Rack");
            list.AddRange(Configuration.Rack.Rows);
            UIUtils.FillCombo(combo, list, 0);
        }

        public static void FillCombo(ComboBox combo, List<string> values, int selected)
        {
            FillCombo(combo, values.ToArray(), selected);
        }

        public static void FillCombo(ComboBox combo, string[] values, int selected)
        {
            for (int i = 0; i < values.Length; i++)
            {
                combo.Items.Add(values[i]);
            }

            combo.SelectedIndex = selected;
        }
    }
}
