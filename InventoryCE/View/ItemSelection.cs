﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using NLog;

namespace InventoryCE.View
{
    public partial class ItemSelection : Form
    {
        private DataGridCell _currentCell;
        private Logger log = LogManager.GetLogger("ItemSelection");
        private DataTable _table = null;

        private string _itemCode = "";
        private string _itemName = "";

        public string ItemCode { get { return _itemCode; } }
        public string ItemName { get { return _itemName; } }

        public ItemSelection(DataTable itemList)
        {
            DialogResult = DialogResult.Cancel;

            if (itemList != null && itemList.Rows.Count > 0)
            {
                _table = itemList;
                this.Width = Program.Width;
                this.Height = Program.Height;
                InitializeComponent();
                dgItemList.DataSource = itemList;
            }
            else
            {
                log.Error("Constructor - itemList was either null or empty.");
            }
        }

        private void dgItemList_CurrentCellChanged(object sender, EventArgs e)
        {
            log.Debug("dgItemList_CurrentCellChanged - Current Cell: [{0},{1}]. Current Row Index: {2}", _currentCell.ColumnNumber, _currentCell.RowNumber, dgItemList.CurrentRowIndex);
            _currentCell = ((DataGrid)sender).CurrentCell;
        }

        private void ItemSelection_Closing(object sender, CancelEventArgs e)
        {
            int rowIndex = dgItemList.CurrentRowIndex;

            if (rowIndex >= 0 && DialogResult == DialogResult.Yes)
            {
                int i = 0;

                try
                {
                    _itemName = dgItemList[rowIndex, 0].ToString();
                    _itemCode = dgItemList[rowIndex, 1].ToString();
                    DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    log.Error("Error retrieving Item information: {0}", ex);
                    log.Debug("Current index: {0}", i);
                }
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void dgItemList_MouseUp(object sender, MouseEventArgs e)
        {
            /*System.Windows.Forms.DataGrid.HitTestInfo hitInfo = dgItemList.HitTest(e.X, e.Y);
            log.Debug("dgItemList_MouseUp - HitTestType: {0}", hitInfo.Type.ToString());*/
        }
    }
}