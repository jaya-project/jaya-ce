﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using InventoryCE.Controller;
using InventoryCE.Model;
using InventoryCE.Model.DbModel;
using InventoryLibraries.SQLite;
using InventoryLibraries.Util;
using NLog;
using System.Text;

namespace InventoryCE.View
{
    /// <summary>
    /// Lets the user extract products from a specific container.
    /// </summary>
    public partial class ProductExtraction : Form
    {
        private Logger log = LogManager.GetLogger("ProductExtraction");
        private SQLiteWrapper _wrap = SQLiteWrapper.GetInstance(Configuration.DBFile);
        private TwContainer _container = null;
        private List<Warehouse> _whList = new List<Warehouse>();
        private XElement _xdoc = null;
        private XElement existing = null;

        public ProductExtraction(string bcdata)
        {
            if (bcdata != null && bcdata.Length > 0)
            {
                InitializeComponent();
                tbBarcode.Text = bcdata;
                ReadData();
                FillWarehouseComboBox();
                FillRackCB();
                SetMaxValues();
                SetInitialData();
            }
            else
            {
                MessageBox.Show("No se leyó el código de barras. Revise el registro de errores para mayor información.");
                log.Error("The barcode data was null or empty {0}.", (bcdata == null ? "null" : bcdata));
            }
        }

        #region Events

        private void nItemToDraw_ValueChanged(object sender, EventArgs e)
        {
            double newCount = _container.ItemCount - Convert.ToDouble(nItemToDraw.Value);
            tbItemCount.Text = newCount.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            StoreData();
        }

        #endregion Events

        /// <summary>
        /// Validates user input.
        /// </summary>
        /// <returns>True if the user input is valid, false otherwise.</returns>
        private bool ValidateData()
        {
            bool r = true;

            if (cbWarehouse.SelectedIndex < 1)
            {
                Utils.SetInvalidControl(cbWarehouse);
                r = false;
            }
            else
            {
                Utils.SetValidControl(cbWarehouse);
            }

            return r;
        }

        /// <summary>
        /// Stores the data to the main XElement object.
        /// </summary>
        private void StoreData()
        {
            if (ValidateData())
            {
                if (_container != null)
                {
                    log.Debug("StoreData() --->");

                    try
                    {
                        Warehouse wh = GetSelectedWarehouse();

                        if (wh != null)
                        {
                            Location location = null;

                            if (cbRow.SelectedIndex > 0)
                            {
                                location = GetLocation(wh, cbRow.SelectedItem.ToString(), nColumn.Value.ToString(), nDepth.Value.ToString());
                            }

                            _container.ItemCount = int.Parse(tbItemCount.Text);
                            UserContainer uc = new UserContainer();
                            uc.Action = UserContainer.ActionName.Modify;
                            uc.UserId = SessionController.CurrentUser.Id;
                            uc.User = SessionController.CurrentUser;
                            uc.ActionDate = DateTime.Now;
                            uc.ContainerId = _container.Id;
                            uc.Container = _container;

                            log.Debug("UserContainer: {0}, {1}, {2}", uc.Action, uc.ActionDate, uc.ContainerId);

                            if (existing != null)
                            {
                                _xdoc = existing;
                                _xdoc.Element("ItemCount").Value = _container.ItemCount.ToString();
                                existing.Remove();
                            }
                            else
                            {
                                _xdoc = _container.ToXml(uc.ToXml());
                            }

                            _xdoc.Add(new XElement("Action",
                                new XElement("ContainerId", _container.Id),
                                new XElement("WarehouseId", wh.Id),
                                (location != null ? location.ToXml() : new XElement("Location")),
                                new XElement("ItemsMoved", nItemToDraw.Value)));

                            log.Debug("XElement: {0}", _xdoc);
                            Main.xdoc.Element("PalletModifications").Add(_xdoc);
                            _container = null;
                            uc = null;
                            wh = null;
                            MessageBox.Show("Información guardada.", "FIN");
                            Close();
                        }
                        else
                        {
                            log.Error("Failed to retrieve selected warehouse.");
                            MessageBox.Show("No se pudo guardar la información. Revise el registro de errores.", "Error");
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Ocurrió un error al guardar la información. Revise el registro de errores.");
                        log.Error("An error ocurred trying to save data to disk: {0}", e);
                    }
                }
                else
                {
                    log.Error("_container was null.");
                }
            }
            else
            {
                MessageBox.Show("Hay errores en los valores ingresados. Revise la información ingresada y vuelva a intentarlo.");
            }
        }

        /// <summary>
        /// Fetches the currently selected warehouse from the Warehouse ComboBox.
        /// </summary>
        /// <returns>The selected warehouse if any or null on failure.</returns>
        private Warehouse GetSelectedWarehouse()
        {
            Warehouse wh = null;

            try
            {
                string[] split = cbWarehouse.SelectedItem.ToString().Split('-');

                if (split.Length == 2)
                {
                    string whname = split[1].Trim(),
                        whcode = split[0].Trim();
                    wh = _whList.Single(warehouse => warehouse.Name == whname && warehouse.WarehouseCode.Code == whcode);
                }
                else
                {
                    log.Error("Could not split selected warehouse code: {0} {1}", cbWarehouse.SelectedText, cbWarehouse.SelectedIndex);
                }
            }
            catch (Exception e)
            {
                log.Error("Could not retrieve warehouse information: {0}", e);
            }

            return wh;
        }

        #region Inital Setup

        /// <summary>
        /// Sets the initial data on the controls.
        /// </summary>
        private void SetInitialData()
        {
            if (_container != null)
            {
                log.Debug("SetInitialData() --->");
                tbProductName.Text = _container.Item.Name;
                // We substract one to keep the tbItemCount updated with the remaining items.
                tbItemCount.Text = (_container.ItemCount - 1).ToString();
                lblMaxRoll.Text = string.Format("Max: {0}", _container.ItemCount.ToString());
                tbPerItemAvgLength.Text = _container.PerItemAvgLength.ToString();
            }
        }

        /// <summary>
        /// Fills the warehouse combo box.
        /// </summary>
        private void FillWarehouseComboBox()
        {
            DataTable dt = null;
            try
            {
                dt = _wrap.Query(
                    "WarehouseView",
                    "(whCode||' - '||whName) AS RowValue, *",
                    "");

                if (dt != null && dt.Rows.Count > 0)
                {
                    cbWarehouse.Items.Add("Seleccione una bodega");
                    cbWarehouse.SelectedIndex = 0;

                    foreach (DataRow row in dt.Rows)
                    {
                        WarehouseCode whcode = new WarehouseCode();
                        whcode.Id = long.Parse(row["warehouseCodeId"].ToString());
                        whcode.Code = row["whCode"].ToString();
                        whcode.Description = row["description"].ToString();

                        Warehouse wh = new Warehouse();
                        wh.Id = long.Parse(row["warehouseId"].ToString());
                        wh.Name = row["whName"].ToString();
                        wh.Size = row["whSize"].ToString();
                        wh.WarehouseCode = whcode;

                        if (!Configuration.HideCurrentWarehouse || (Configuration.HideCurrentWarehouse && _container.Location.Warehouse.Id != wh.Id))
                        {
                            cbWarehouse.Items.Add(row["RowValue"]);
                            _whList.Add(wh);
                        }

                        whcode = null;
                        wh = null;
                    }
                }
                else
                {
                    log.Error("Could not set the values for Warehouse Code!");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }
        }

        /// <summary>
        /// Reads and fills the _container field.
        /// </summary>
        private void ReadData()
        {
            try
            {
                log.Debug("ReadData() --->");
                _container = DecodeBarcode();

                if (_container != null)
                {
                    int itemCount = GetItemCountFromXml(_container.Identifier, _container.LotCode);

                    if (itemCount != 0)
                    {
                        _container = FillContainerFromDb(_container.Identifier, _container.LotCode);

                        if (_container == null)
                        {
                            MessageBox.Show("No se pudo extraer la información del contenedor. Revise el registro de errores para mayor información.", "Error de lectura");
                            Close();
                        }
                        else
                        {
                            if (itemCount > 0)
                                _container.ItemCount = itemCount;
                        }
                    }
                    else
                    {
                        MessageBox.Show("A este pallet ya no le quedan rollos. Por favor elija otro.", "Pallet Vacío");
                        Close();
                    }
                }
                else
                {
                    MessageBox.Show("No se pudo descifrar el código de barras. Revise el registro de errores.");
                    Close();
                }
            }
            catch (Exception e)
            {
                log.Error("Error reading container information: {0}", e);
                MessageBox.Show("No se pudo extraer la información del contenedor. Revise el registro de errores para mayor información.", "Error Base de Datos");
                Close();
            }
        }

        /// <summary>
        /// Retrieves the current item count inside the container (identified by identifier and lot code) from the xml
        /// data object.
        /// </summary>
        /// <param name="identifier">The container identifier.</param>
        /// <param name="lotcode">The container lot code.</param>
        /// <returns>A number >= 0 if there is no error or -1 if something failed. 0 means that there are no more items
        /// inside this container, while any number above 0 is the current item count inside it.</returns>
        private int GetItemCountFromXml(long identifier, string lotcode)
        {
            int itemCount = -1;

            if (Main.xdoc.Element("PalletModifications").HasElements)
            {
                List<XElement> list = (from node in Main.xdoc.Element("PalletModifications").Elements("Container")
                                       where long.Parse(node.Element("Identifier").Value) == identifier &&
                                             node.Element("LotCode").Value == lotcode
                                       select node).ToList();
                try
                {
                    if (list != null && list.Count == 1)
                    {
                        existing = list.First();
                        log.Debug("Container found xml data: {0}", existing);
                        log.Debug("There is a container with identifier = {0} and lot code = {1}", identifier, lotcode);
                        itemCount = int.Parse(existing.Element("ItemCount").Value);

                        if (itemCount < 0)
                        {
                            log.Warn("The item count for container [identifier={0}, lot code={1}] was less than 0.", identifier, lotcode);
                            itemCount = 0;
                        }
                    }
                    else
                    {
                        log.Error("The Container element in the XML file was either null or had more than one element (element count: {0}) with the same key [idenfifier/lot code].", list.Count);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Failed to retrieve container information from XML: {0}", ex);
                }
            }

            log.Debug("Container found item count = {0}.", itemCount);

            return itemCount;
        }

        /// <summary>
        /// Fills the container from the database.
        /// </summary>
        /// <param name="identifier">Container identifier.</param>
        /// <param name="lotcode">Container lot code.</param>
        /// <returns>A TwContainer object on success or null on failure.</returns>
        private TwContainer FillContainerFromDb(long identifier, string lotcode)
        {
            DataTable dt = null;
            TwContainer container = null;

            try
            {
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("identifier = {0} AND lotCode = {1}", new object[] { identifier, lotcode });
                dt = _wrap.Query("ContainerView", "", "", where, 1, 0);
                where = null;

                if (dt != null && dt.Rows.Count == 1)
                {
                    DataRow r = dt.Rows[0];
                    container = new TwContainer();
                    container.Id = long.Parse(r["containerId"].ToString());
                    container.ItemCount = int.Parse(r["itemCount"].ToString());
                    container.ItemTotalLength = Convert.ToDouble(r["itemTotalLength"].ToString());
                    container.PerItemAvgLength = double.Parse(r["perItemAvgLength"].ToString());
                    container.UnitPrice = Convert.ToDouble(r["unitPrice"].ToString());
                    container.Identifier = identifier;
                    container.LotCode = lotcode;

                    if (_container.Item == null)
                    {
                        container.Item = new InventoryCE.Model.DbModel.Item();
                        container.Item.Id = long.Parse(r["itemId"].ToString());
                        container.Item.Code = r["itemCode"].ToString();
                        container.Item.Name = r["itemName"].ToString();
                    }

                    String row = r["row"].ToString(),
                    column = r["column"].ToString(),
                    depth = r["depth"].ToString();

                    container.Location = new InventoryCE.Model.DbModel.Location();
                    container.Location.Id = long.Parse(r["locationId"].ToString());
                    container.LocationId = container.Location.Id;
                    container.Location.Row = row;
                    container.Location.Column = column;
                    container.Location.Depth = depth;
                    container.Location.Warehouse = new Warehouse();
                    container.Location.Warehouse.Id = long.Parse(r["warehouseId"].ToString());
                    container.Location.Warehouse.Name = r["whName"].ToString();
                    container.Location.Warehouse.Size = r["whSize"].ToString();
                    container.Location.Warehouse.WarehouseCode = new WarehouseCode();
                    container.Location.Warehouse.WarehouseCode.Id = long.Parse(r["warehouseCodeId"].ToString());
                    container.Location.Warehouse.WarehouseCode.Code = r["whCode"].ToString();
                    container.Location.Warehouse.WarehouseCode.Description = r["description"].ToString();

                    r = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Failed to read container data from database: {0}", ex);
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }

            return container;
        }

        /// <summary>
        /// Fills the rack combo box.
        /// </summary>
        private void FillRackCB()
        {
            try
            {
                UIUtils.FillRowCombo(cbRow);
            }
            catch (Exception e)
            {
                log.Error("Could not fill rack data: {0}", e);
            }
        }

        /// <summary>
        /// Sets the maximum values for the numeric controls based on user input.
        /// </summary>
        private void SetMaxValues()
        {
            try
            {
                nColumn.Maximum = decimal.Parse(Configuration.Rack.MaxColumns);
                nDepth.Maximum = decimal.Parse(Configuration.Rack.MaxDepth);
            }
            catch (Exception e)
            {
                log.Error("Error trying to set maximum column and depth values: {0}", e);
            }
            finally
            {
                nColumn.Maximum = 100;
                nDepth.Maximum = 100;
                nItemToDraw.Maximum = Convert.ToDecimal(_container.ItemCount);
            }
        }

        /// <summary>
        /// Parses the barcode and fills the Container data with it.
        /// </summary>
        /// <returns>A TwContainer with the barcode data on it.</returns>
        private TwContainer DecodeBarcode()
        {
            TwContainer container = null;
            log.Debug("DecodeBarcode() --->");
            BarcodeParser parser = new BarcodeParser();

            try
            {
                container = parser.Parse(tbBarcode.Text);
            }
            catch (FormatException ex)
            {
                log.Error("DecodeBarcode - Exception caught: ", ex);
            }

            if (container == null)
            {
                StringBuilder sb = new StringBuilder();

                foreach (KeyValuePair<int, string> kvp in parser.ErrorList)
                {
                    if (kvp.Key == BarcodeParser.ERROR_INVALID_LOTCODE)
                    {
                        sb.Append("El número de lote no es válido.");
                        log.Error("Error: {0}", kvp.Value);
                    }

                    if (kvp.Key == BarcodeParser.ERROR_INVALID_LOTID)
                    {
                        sb.Append("El identificador de pallet no es válido.");
                        log.Error("Error: {0}", kvp.Value);
                    }

                    if (kvp.Key == BarcodeParser.ERROR_INVALID_LOCATION)
                    {
                        sb.Append("La ubicación del pallet no es válida. Una ubicación válida es, por ejemplo: A0000 (sin espacios. Sólo una letra y números).");
                        log.Error("Error: {0}", kvp.Value);
                    }
                }

                if (sb.Length > 0)
                {
                    MessageBox.Show("Ocurrió un error al traducir el código de barras:\n\n" + sb.ToString());
                    sb = null;
                    this.Close();
                }
                else
                {
                    log.Warn("Nothing happened when parsing the barcode.");
                }
            }

            return container;
        }

        /// <summary>
        /// Retrieves a Location based on the row, column and depth.
        /// </summary>
        /// <param name="warehouse">The Warehouse to where this location is.</param>
        /// <param name="row">Location row.</param>
        /// <param name="column">Location Column.</param>
        /// <param name="depth">A Location Depth.</param>
        /// <returns>A location object. This function never returns null.</returns>
        private Location GetLocation(Warehouse warehouse, string row, string column, string depth)
        {
            DataTable dt = null;
            Location location = new Location();
            location.Row = row;
            location.Column = column;
            location.Depth = depth;
            location.Warehouse = warehouse;
            location.WarehouseId = warehouse.Id;

            try
            {
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("row = {0} AND column = {1} AND depth = {2} AND warehouseId = {3}", new object[] { row, column, depth, warehouse.Id });
                dt = _wrap.Query("Location", "*", "", where, 1, 0);
                where = null;

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow r = dt.Rows[0];
                    location.Id = long.Parse(r["locationId"].ToString());
                    location.Name = r["locationName"].ToString();
                    location.Position = r["position"].ToString();
                }
            }
            catch (Exception ex)
            {
                log.Error("Failed to read location [row={0}, column={1}, depth={2}, warehouse id={3}] data: {4}", row, column, depth, warehouse.Id, ex);
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();

                dt = null;
            }

            return location;
        }

        #endregion Initial Setup

        /// <summary>
        /// Converts an object to long.
        /// </summary>
        /// <param name="o">The object that will be converted.</param>
        /// <returns>The long representation of 'o' or 0L</returns>
        private long Long(Object o)
        {
            String s = String(o);

            if (s.Length > 0)
                return long.Parse(s);

            return 0L;
        }

        /// <summary>
        /// Converts an object to int.
        /// </summary>
        /// <param name="o">Object that will be converted.</param>
        /// <returns>An integer representation of 'o' or 0</returns>
        private int Int(Object o)
        {
            String s = String(o);

            if (s.Length > 0)
                return int.Parse(s);

            return 0;
        }

        /// <summary>
        /// Converts an object to String without throwing a exception if the object is null.
        /// </summary>
        /// <param name="o">The Object that will be converted.</param>
        /// <returns>The String representation of 'o' or an empty String.</returns>
        private String String(Object o)
        {
            if (o != null)
                return o.ToString();

            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event arguments</param>
        private void ProductExtraction_Closing(object sender, CancelEventArgs e)
        {
            OnFormClosing();
        }

        /// <summary>
        /// Handles the cleaning routine.
        /// </summary>
        private void OnFormClosing()
        {
            log = null;
            _wrap = null;
            _container = null;
            _whList = null;
            _xdoc = null;
            existing = null;
        }
    }
}