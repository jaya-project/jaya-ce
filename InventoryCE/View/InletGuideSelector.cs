﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Xml.Linq;
using NLog;

namespace InventoryCE.View
{
    public partial class InletGuideSelector : Form
    {
        private Logger log = LogManager.GetLogger("InletGuideSelector");
        private IEnumerable<XElement> _list = null;
        private XElement _guide = null;

        public XElement Guide { get { return _guide; } }

        public InletGuideSelector(IEnumerable<XElement> list)
        {
            InitializeComponent();

            if (list != null && list.Count() > 1)
            {
                _list = list;
                FillDataGrid();
            }
            else
            {
                log.Error("Inlet guide list was null or had less than 2 elements.");
            }
        }

        private void FillDataGrid()
        {
            DataSet dset = new DataSet("inletGuides");
            DataTable table = new DataTable("inletGuide");
            DataColumn column = new DataColumn("lotCode", typeof(string));
            DataRow row;
            table.Columns.Add(column);
            dset.Tables.Add(table);

            foreach (XElement element in _list)
            {
                row = table.NewRow();
                row["lotCode"] = element.Attribute("lotCode").Value;
                table.Rows.Add(row);
            }

            dgGuides.DataSource = dset.Tables["inletGuide"];

            DataGridTableStyle style = new DataGridTableStyle();
            style.MappingName = "inletGuide";
            DataGridColumnStyle cstyle = new DataGridTextBoxColumn();
            cstyle.HeaderText = "Lote";
            cstyle.MappingName = "lotCode";
            cstyle.Width = 232;
            style.GridColumnStyles.Add(cstyle);

            dgGuides.TableStyles.Add(style);

            style = null;
            cstyle = null;
            row = null;
            table = null;
            column = null;
            dset = null;
        }



        private void btnSelect_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void InletGuideSelector_Closing(object sender, CancelEventArgs e)
        {
            int rowIndex = dgGuides.CurrentRowIndex;

            if (rowIndex >= 0 && DialogResult == DialogResult.OK)
            {
                string lotCode = dgGuides[rowIndex, 0].ToString();
                log.Debug("Selected lot Code: {0}", lotCode);
                XElement element = (from nodes in _list
                                    where nodes.Attribute("LotCode").Value == lotCode
                                    select nodes).ToList().FirstOrDefault();

                log.Debug("Selected XElement guide: {0}", element);

                if (element != null)
                {
                    _guide = element;
                }
                else
                {
                    log.Debug("Failed to select an XElement: ", element);
                    DialogResult = DialogResult.Abort;
                }
            }
            else
            {
                log.Debug("Either row index was LT 1 or DialogResult was not OK. Row Index: {0}", rowIndex);
                DialogResult = DialogResult.Abort;
            }
        }
    }
}