﻿namespace InventoryCE.View
{
    partial class BCScan
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.manualInput = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnHash = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(10, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 20);
            this.label1.Text = "Instrucciones";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(10, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 51);
            this.label2.Text = "1.- Presione cualquiera de los 4 botones de color naranjo en el lector.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(10, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(213, 53);
            this.label3.Text = "2.- Sin soltar el botón, dirija el láser hacia el código que desea escanear.";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(10, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 36);
            this.label4.Text = "3.- Oirá un sonido y luego debe tocar sobre Ok.";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(10, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(213, 20);
            this.label5.Text = "-------------------------------------------";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(10, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 20);
            this.label6.Text = "Ingreso Manual";
            // 
            // manualInput
            // 
            this.manualInput.Location = new System.Drawing.Point(11, 208);
            this.manualInput.Name = "manualInput";
            this.manualInput.Size = new System.Drawing.Size(160, 23);
            this.manualInput.TabIndex = 6;
            this.manualInput.TextChanged += new System.EventHandler(this.manualInput_TextChanged);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnOk.Enabled = false;
            this.btnOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnOk.Location = new System.Drawing.Point(177, 208);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(46, 23);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnHash
            // 
            this.btnHash.BackColor = System.Drawing.Color.Peru;
            this.btnHash.ForeColor = System.Drawing.Color.White;
            this.btnHash.Location = new System.Drawing.Point(149, 182);
            this.btnHash.Name = "btnHash";
            this.btnHash.Size = new System.Drawing.Size(74, 20);
            this.btnHash.TabIndex = 14;
            this.btnHash.Text = "Insertar";
            this.btnHash.Click += new System.EventHandler(this.btnHash_Click);
            // 
            // BCScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 239);
            this.Controls.Add(this.btnHash);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.manualInput);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BCScan";
            this.Text = "Escaneo de Código";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.BCScan_Closing);
            this.Resize += new System.EventHandler(this.BCScan_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox manualInput;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnHash;
    }
}