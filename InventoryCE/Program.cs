﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using NLog;

namespace InventoryCE
{
    static class Program
    {
        [DllImport("coredll.dll", SetLastError = true)]
        public static extern IntPtr CreateMutex(IntPtr lpMuttexAttributes, bool bInitialOwner, string lpName);

        [DllImport("coredll.dll")]
        public static extern bool ReleaseMutex(IntPtr hMuttex);

        public const int Height = 295;
        public const int Width = 240;
        private static Logger log = LogManager.GetLogger("Control de Inventario");
        public const int ERROR_ALREADY_EXISTS = 183;

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [MTAThread]
        static void Main(string[] args)
        {
            IntPtr ipMutexAtt = new IntPtr(0);
            IntPtr ipHMutex = new IntPtr(0);
            View.Main mainForm = null;

            try
            {
                ipHMutex = CreateMutex(ipMutexAtt, false, "ValorTI_ControlDeInventario_Mutex");

                if (ipHMutex != IntPtr.Zero)
                {
                    int lastError = Marshal.GetLastWin32Error();

                    if (lastError == ERROR_ALREADY_EXISTS)
                    {
                        MessageBox.Show("Ya abrió el programa una vez. Por favor espere a que aparezca la pantalla principal.", "Aplicación Abierta");
                        return;
                    }
                }
                else
                {
                    log.Error("Failed to create application muttex.");
                }

                mainForm = new InventoryCE.View.Main(args);
                Application.Run(mainForm);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error en la aplicación principal. Revise el registro de errores y póngase en contacto con el soporte técnico.", "Error");
                log.Error("Exception caught in main thread: {0}", e);
            }
            finally
            {
                if (ipHMutex != (IntPtr)0)
                {
                    ReleaseMutex(ipHMutex);
                }

                if (mainForm != null)
                {
                    mainForm.Dispose();
                }
            }
        }
    }
}