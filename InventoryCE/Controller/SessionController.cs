﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using InventoryCE.Model;
using InventoryCE.Model.DbModel;
using InventoryLibraries.Sessions;
using InventoryLibraries.SQLite;
using InventoryLibraries.Util;
using NLog;

namespace InventoryCE.Controller
{
    public static class SessionController
    {
        public delegate void ProgressUpdate(object sender, ProgressArgs e);
        public static event ProgressUpdate UpdateProgress;

        public enum ErrorCode
        {
            None = 1, Success, InvalidCredentials, Error, SessionNotSaved
        }

        private static User User;
        private static Session Session;
        private static byte LoggedIn = 0;
        private static Logger log = LogManager.GetLogger("SessionController");
        private static SQLiteWrapper Sqlite = SQLiteWrapper.GetInstance(Configuration.DBFile);
        private static bool Expire = Configuration.Session.Expires;
        private static int Duration = Configuration.Session.Duration;
        private static ProgressArgs pa = new ProgressArgs();
        private static ErrorCode Code = ErrorCode.None;

        public static User CurrentUser { get { return User; } }

        public static Session CurrentSession { get { return Session; } }

        public static bool SessionExpire
        {
            get
            {
                return Expire;
            }
        }

        public static int SessionDuration
        {
            get
            {
                return Duration;
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                return LoggedIn == 1;
            }
        }

        public static DateTime LastActive
        {
            get;
            set;
        }

        public static ErrorCode Authenticate(string userName, string password)
        {
            try
            {
                Code = ErrorCode.None;
                pa.Value = 1;
                OnUpdate(pa);
                User = CreateUser(userName);

                if (User != null && User.Can("use_barcode_reader"))
                {
                    pa.Value += 30;
                    OnUpdate(pa);

                    SessionHelper sh = new SessionHelper();
                    byte[] hash = sh.MakeHash(password, User.Salt);

                    if (sh.CompareHash(hash, User.Password))
                    {
                        pa.Value += 20;
                        OnUpdate(pa);

                        if (StartSession())
                        {
                            Code = ErrorCode.Success;
                        }
                    }
                    else
                    {
                        pa.Value = 1;
                        OnUpdate(pa);

                        Code = ErrorCode.InvalidCredentials;
                        log.Warn("Invalid login attempt. User: [{0}]", User.NickName);
                    }

                    sh = null;
                }
                else
                {
                    Code = ErrorCode.Error;
                    log.Error("Could not create a new user object.");
                    log.Trace("User = {0}", User);
                }
            }
            catch (Exception e)
            {
                Code = ErrorCode.Error;
                log.Error("Authentication failed: {0}", e);
            }

            return Code;
        }

        private static bool StartSession()
        {
            try
            {
                DateTime now = DateTime.Now;
                Session = new Session(now, Session.SessionEvent.Login, User, DeviceUtils.GetDeviceID(), DeviceUtils.GetDeviceName());

                if (Session != null)
                {
                    log.Info("Session Start: {0} - {1} - {2}", now.ToString(CultureInfo.CurrentCulture), Session.EventName.ToString(), User.NickName);
                    pa.Value += 50;
                    OnUpdate(pa);
                    LoggedIn = 1;
                    return true;
                }
            }
            catch (Exception e)
            {
                pa.Value = 1;
                OnUpdate(pa);
                Code = ErrorCode.Error;
                log.Error("Session start error: {0}", e);
            }

            return false;
        }

        public static void UpdateSession()
        {
            if (Expire)
            {
                try
                {
                    DateTime newDate = Session.EventTime;
                    newDate.AddMinutes((double)Duration);

                    if (newDate.CompareTo(Session.EventTime) > 0)
                    {
                        Logout(newDate);
                    }
                }
                catch (Exception e)
                {
                    log.Error("Error trying to update session data: {0}", e);
                }
            }
        }

        public static bool Logout(DateTime date)
        {
            if (date == null)
                date = DateTime.Now;

            try
            {
                Session = null;
                Session = new Session(date, Session.SessionEvent.Logout, User, DeviceUtils.GetDeviceID(), DeviceUtils.GetDeviceName());

                if (Session != null)
                {
                    log.Info("Session End: {0} - {1} - {2}", Session.EventTime.ToString(CultureInfo.CurrentCulture), Session.EventName.ToString(), User.NickName);
                    LoggedIn = 0;
                    Session = null;
                    User = null;
                    return true;
                }
                else
                {
                    log.Error("Could not Logout!");
                }
            }
            catch (Exception e)
            {
                log.Error("An error ocurred trying to logout: {0}", e);
            }

            return true;
        }

        private static void OnUpdate(ProgressArgs e)
        {
            ProgressUpdate handler = UpdateProgress;

            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static User CreateUser(string nickname)
        {
            try
            {
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("nickname = {0}", nickname);
                DataTable dt = Sqlite.Query("Employee", "*", "", where, 1, 0);
                where = null;

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    dt = null;
                    User u = new User(
                        long.Parse(row["employeeId"].ToString()),
                        row["firstName"].ToString(),
                        row["lastName"].ToString(),
                        row["rut"].ToString(),
                        row["nickname"].ToString(),
                        (byte[])(row["password"]),
                        (byte[])(row["salt"])
                    );

                    StringBuilder join = new StringBuilder("INNER JOIN ");
                    join.AppendFormat("{0} ON EmployeeCapabilities.capabilityId = capability.capabilityId", "Capability");
                    where = new SQLiteWrapper.SQLWhere();
                    where.Append("employeeId = {0}", u.Id);
                    dt = Sqlite.Query(
                        "EmployeeCapabilities",
                        "capability.capabilityId CapId, name CapName, capability.description CapDesc",
                        join.ToString(),
                        where);
                    where = null;
                    join = null;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        List<Capability> caps = new List<Capability>();

                        foreach (DataRow r in dt.Rows)
                        {
                            caps.Add(new Capability(
                                long.Parse(r["CapId"].ToString()),
                                r["CapName"].ToString(),
                                r["CapDesc"].ToString()));
                        }

                        u.Capabilities = caps;
                        return u;
                    }
                    else
                    {
                        log.Error("Could not retrieve capability list! Rows retrieved: {0}", dt.Rows.Count);
                    }

                    row = null;
                }

                dt = null;
            }
            catch (Exception e)
            {
                log.Error("Could not create a user object: {0}", e);
            }

            return null;
        }
    } // End class LoginController
}
