﻿using System;

namespace InventoryCE.Model
{
    public class StringUtils
    {
        public static int GetInt(Object o)
        {
            String s = GetString(o);

            if (s == "")
                return 0;

            return int.Parse(s);
        }

        public static long GetLong(Object o)
        {
            String s = GetString(o);

            if (s == "")
                return 0;

            return long.Parse(s);
        }

        public static String GetString(Object o)
        {
            if (o != null)
                return o.ToString();

            return "";
        }

        public static double GetDouble(Object o)
        {
            String s = GetString(o);

            if (s == "")
                return 0.0;

            return double.Parse(s);
        }
    }
}
