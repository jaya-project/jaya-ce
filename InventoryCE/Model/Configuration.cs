﻿using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using InventoryLibraries.Util;
using System.Collections.Generic;

namespace InventoryCE.Model
{
    public static class Configuration
    {
        #region Members

        private static XElement _root = XDocument.Load(Path.Combine(Utils.BasePath, "Configuration.xml")).Element("Configuration");

        #region Static Values

        private static string _externalStoragePath = _root.Element("Settings").Element("ExternalStorage").Value;
        private static string _storageFileName = _root.Element("Settings").Element("StorageFileName").Value;
        private static string _dbFileName = _root.Element("Settings").Element("DbFile").Value;
        private static string _dbFile = string.Format("{0}{1}", _externalStoragePath, _dbFileName);
        private static bool _hideCurrentWarehouse = bool.Parse(_root.Element("Settings").Element("HideCurrentWarehouse").Value);

        #endregion Static Values

        #endregion Members

        #region Getters

        public static string ExternalStoragePath { get { return _externalStoragePath; } }
        public static string DBFile { get { return _dbFile; } }
        public static string DBFileName { get { return _dbFileName; } }
        public static string StorageFileName { get { return _storageFileName; } }
        public static bool HideCurrentWarehouse { get { return _hideCurrentWarehouse; } }

        #endregion Getters

        #region Nested Classes

        public static class Session
        {
            private static bool _expires = bool.Parse(_root.Element("Session").Element("Expires").Value);
            private static int _duration = int.Parse(_root.Element("Session").Element("Duration").Value);

            public static bool Expires
            {
                get
                {
                    return _expires;
                }
            }

            public static int Duration
            {
                get
                {
                    return _duration;
                }
            }
        }

        public static class Rack
        {
            private static string[] _rows = _root.Element("Location").Element("Rows").Value.Split(',');
            private static string _maxColumns = _root.Element("Location").Element("MaxColumns").Value;
            private static string _maxDepth = _root.Element("Location").Element("MaxDepth").Value;

            public static string[] Rows
            {
                get
                {
                    return _rows;
                }
            }

            public static string MaxColumns
            {
                get
                {
                    return _maxColumns;
                }
            }

            public static string MaxDepth
            {
                get
                {
                    return _maxDepth;
                }
            }
        }

        public static class Barcode
        {
            private static string _removeBarcodeChars = _root.Element("Barcode").Element("RemoveBarcodeChars").Value;
            private static string _barcodeSeparator = _root.Element("Barcode").Element("BarcodeSeparator").Value;
            private static string _format = _root.Element("Barcode").Element("Format").Value;

            public static string RemoveBarcodeChars { get { return _removeBarcodeChars; } }
            public static string BarcodeSeparator { get { return _barcodeSeparator; } }
            public static string Format { get { return _format; } }
        }

        #endregion Nested Classes
    }
}
