﻿
namespace InventoryCE.Model
{
    public class ProgressArgs
    {
        public int InitialValue { get; set; }

        public int MaxValue { get; set; }

        public int Value { get; set; }
    }
}
