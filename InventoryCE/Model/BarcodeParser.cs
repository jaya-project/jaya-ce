﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using NLog;
using InventoryCE.Model.DbModel;
using InventoryLibraries.Util;
using System.Text.RegularExpressions;

namespace InventoryCE.Model
{
    public class BarcodeParser
    {
        public const int ERROR_INVALID_LOTCODE = 100;
        public const int ERROR_INVALID_LOTID = 101;
        public const int ERROR_INVALID_LOCATION = 102;

        private Logger log = LogManager.GetLogger("BarcodeParser");
        private Dictionary<int, string> errorList = new Dictionary<int, string>();
        public Dictionary<int, string> ErrorList { get { return errorList; } }

        /// <summary>
        /// Parses the barcode information and creates a container object with the data.
        /// </summary>
        /// <param name="bcData">The clean data read by the scanner.</param>
        /// <returns>A TwContainer object if the data is valid or null if it's not.</returns>
        public TwContainer Parse(string bcData)
        {
            string[] split = bcData.Split(Configuration.Barcode.BarcodeSeparator.ToCharArray());

            if (Validate(split))
            {
                TwContainer container = new TwContainer();
                container.LotCode = split[0];
                container.Identifier = long.Parse(split[1]);
                container.Location = new Location();
                container.Location.Row = split[2].Substring(0, 1);
                container.Location.Column = split[2].Substring(1, 2);
                container.Location.Depth = split[2].Substring(3, 2);

                return container;
            }

            return null;
        }

        private bool Validate(string[] split)
        {
            bool r = true;

            if (split.Length >= 3)
            {
                int i = 0;

                for (; i < split.Length; i++)
                    split[i] = split[i].Trim();

                i = 0;
                string tmp = split[i];

                if (!Utils.IsNumber(tmp))
                {
                    errorList.Add(ERROR_INVALID_LOTCODE, "The lot code must be a number [" + tmp + "].");
                    r &= false;
                }

                i++;
                tmp = split[i];

                if (!Utils.IsNumber(tmp))
                {
                    errorList.Add(ERROR_INVALID_LOTID, "Lot identification is not a valid number [" + tmp + "]. Use only integer numbers.");
                    r &= false;
                }

                i++;
                tmp = split[i];

                if (!Regex.IsMatch(tmp, @"^[a-zñ]\d{2}\d{2}$", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                {
                    errorList.Add(ERROR_INVALID_LOCATION, "Location is not valid: [" + tmp + "].");
                    r &= false;
                }
            }
            else
            {
                r &= false;
            }

            return r;
        }
    }
}
