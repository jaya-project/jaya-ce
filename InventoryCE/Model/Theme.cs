﻿using System.Drawing;

namespace InventoryCE.Model
{
    public class Theme
    {
        // 244, 234, 210
        private Color formBgColor;


        private Color labelColor;

        public Theme()
        {
            formBgColor = Color.FromArgb(244, 234, 210);
            labelColor = Color.Coral;
        }

        public Color LabelColor { get { return labelColor; } }
        public Color FormBgColor { get { return formBgColor; } }
    }
}
