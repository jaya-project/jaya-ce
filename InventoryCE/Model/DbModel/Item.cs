﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    public class Item
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ForeignProvider ForeignProvider { get; set; }
        private List<Property> _properties = new List<Property>();
        public List<Property> Properties
        {
            get
            { 
                return _properties;
            }

            set
            {
                _properties = value;
            }
        }

        public Item() {}

        public XElement ToXml()
        {
            XElement el = new XElement("Item",
            new XElement("ItemId", Id.ToString()),
            new XElement("ItemName", Name),
            new XElement("ItemCode", Code),
            new XElement("ImportCode", ForeignProvider.Code));

            return el;
        }
    }
}
