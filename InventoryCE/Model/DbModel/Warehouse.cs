﻿using System;
using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    [Serializable]
    public class Warehouse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public WarehouseCode WarehouseCode { get; set; }

        public Warehouse() { }

        public XElement ToXml()
        {
            return new XElement("Warehouse",
                new XElement("WarehouseId", Id.ToString()),
                new XElement("WhName", Name),
                new XElement("WhSize", Size),
                new XElement("WarehouseCodeId", WarehouseCode.Id.ToString()));
        }

        /*public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<warehouse>");
            sb.AppendFormat("<id>{0}</id>", Id);
            sb.AppendFormat("<name>{0}</name>", Name);
            sb.AppendFormat("<size>{0}</size>", Size);
            sb.Append(WarehouseCode.ToString());
            sb.Append("</warehouse>");
            return sb.ToString();
        }*/
    }
}
