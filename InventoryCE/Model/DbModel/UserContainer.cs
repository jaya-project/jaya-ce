﻿using System;
using System.Globalization;
using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    [Serializable]
    public class UserContainer
    {
        public enum ActionName
        {
            Entry, Modify, Move, Inventory
        }

        public long UserId { get; set; }
        public long ContainerId { get; set; }
        public User User { get; set; }
        public TwContainer Container { get; set; }
        public DateTime ActionDate { get; set; }
        public ActionName Action { get; set; }

        public UserContainer() { }

        public XElement ToXml()
        {
            return new XElement("UserContainer",
                new XElement("UserId", UserId.ToString()),
                new XElement("ActionDate", ActionDate.ToString("dd/MM/yyyy HH:mm:ss")),
                new XElement("ActionName", Action.ToString()),
                new XElement("ContainerId", (ContainerId > 0 ? ContainerId.ToString() : "inherit")));
        }

        public string ToCsv()
        {
            return string.Format("UserContainer: user_id={0},container_id={1},action_date={2},action_name={3}{4}",
                UserId, ContainerId, ActionDate.ToString(), Action.ToString(), Environment.NewLine);
        }
    }
}
