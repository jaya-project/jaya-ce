﻿using System;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using InventoryCE.View;
using InventoryLibraries.SQLite;
using System.IO;

namespace InventoryCE.Model.DbModel
{
    public class Session
    {
        public enum SessionEvent
        {
            Login, Logout
        }

        private SQLiteWrapper wrap = SQLiteWrapper.GetInstance(Configuration.DBFile);
        public long Id { get; set; }
        public DateTime EventTime { get; set; }
        public SessionEvent EventName { get; set; }
        public User User { get; set; }
        private string nl = Environment.NewLine;
        public string LogonDeviceId { get; set; }
        public string LogonDeviceName { get; set; }

        public Session() { }

        public Session(long id, DateTime eventTime, SessionEvent eventName, User user, string logonDeviceId, string logonDeviceName)
        {
            Id = id;
            EventName = eventName;
            EventTime = eventTime;
            User = user;
            LogonDeviceId = logonDeviceId;
            LogonDeviceName = logonDeviceName;
        }

        public Session(DateTime eventTime, SessionEvent eventName, User user, string logonDeviceId, string logonDeviceName)
            : this(0, eventTime, eventName, user, logonDeviceId, logonDeviceName)
        {
            StoreSession();
        }

        private bool StoreSession()
        {
            StringBuilder sb = new StringBuilder();
            // event_time, action_name, user_id, logon_device_id, logon_device_name
            sb.AppendFormat("{0},{1},{2},{3},{4}\n",
                EventTime.ToString(CultureInfo.CurrentCulture), EventName.ToString(), User.Id, LogonDeviceId, LogonDeviceName, Environment.NewLine);
            return Main.WriteDataToExternalStorage(sb.ToString(), "session.csv", false);
        }

        public bool Save()
        {
            return StoreSession();
        }

        public XElement ToXml()
        {
            return new XElement("Session",
                new XElement("SessionId", Id.ToString()),
                new XElement("EventTime", EventTime.ToString()),
                new XElement("EventName", EventName.ToString()),
                new XElement("EmployeeId", User.Id.ToString()),
                new XElement("LogonDeviceId", LogonDeviceId),
                new XElement("LogonDeviceName", LogonDeviceName));
        }
    }
}
