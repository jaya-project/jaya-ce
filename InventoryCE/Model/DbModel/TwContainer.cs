﻿using System;
using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    [Serializable]
    public class TwContainer
    {
        public long Id { get; set; }

        public long Identifier { get; set; }

        public string LotCode { get; set; }

        public string Dimensions { get; set; }

        public int Modifications { get; set; }

        private DateTime _expireDate;

        public DateTime ExpireDate
        {
            get
            {
                return _expireDate;
            }

            set
            {
                if (value != null)
                    _expireDate = value;
            }
        }

        public string PieceColor { get; set; }

        public int ItemCount { get; set; }

        public double ItemTotalLength { get; set; }

        public double PerItemAvgLength { get; set; }

        public double UnitPrice { get; set; }

        public long LocationId { get; set; }

        public Location Location { get; set; }

        public long ItemId { get; set; }

        public Item Item { get; set; }

        public TwContainer() { }

        public XElement ToXml(params XElement[] userAction)
        {
            XElement el = new XElement("Container",
                new XElement("ContainerId", (Id > 0 ? Id.ToString() : "generate")),
                new XElement("Identifier", (Identifier > 0 ? Identifier.ToString() : "generate")),
            new XElement("LotCode", LotCode),
            new XElement("Dimensions", Dimensions),
            new XElement("ModCount", Modifications.ToString()),
            new XElement("ExpireDate", ""),//(_expireDate != null ? _expireDate.ToString("d", Configuration.DateCulture) : "")),
            new XElement("PieceColor", PieceColor),
            new XElement("ItemCount", ItemCount.ToString()),
            new XElement("ItemTotalLength", ItemTotalLength.ToString()),
            new XElement("PerItemAvgLength", PerItemAvgLength.ToString()),
            new XElement("UnitPrice", UnitPrice.ToString()),
            new XElement("ItemId", Item.Id.ToString()),
            new XElement("LocationId", Location != null ? Location.Id : 0));

            if (userAction != null && userAction.Length > 0)
            {
                el.Add(userAction);
            }

            return el;
        }

        public string ToCsv()
        {
            return string.Format(
                "Container: id={0},identifier={1},lot_code={2},dimensions={3},mod_count={4},expire_date={5},piece_color={6},item_count={7},per_item_avg_length={8},unit_price={9},location_id={10},inlet_guide_id={11},item_id={12}{13}",
                Id, Identifier, LotCode, Dimensions,
                Modifications, "null", PieceColor,
                ItemCount, ItemTotalLength, PerItemAvgLength,
                UnitPrice, LocationId, ItemId, Environment.NewLine);
        }
    }
}
