﻿using System.Collections.Generic;

namespace InventoryCE.Model.DbModel
{
    public class ItemProperties
    {
        List<Item> Items = new List<Item>();
        List<Property> Properties = new List<Property>();
    }
}
