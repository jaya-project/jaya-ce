﻿using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    public class TwInventory
    {
        public enum Difference
        {
            Ullage, Surplus, None
        }

        public long Id
        {
            get;
            set;
        }

        public TwContainer Container
        {
            get;
            set;
        }

        public Difference DifferenceName
        {
            get;
            set;
        }

        public int DifferenceCount
        {
            get;
            set;
        }

        public string Comments
        {
            get;
            set;
        }

        public TwInventory() { }

        public XElement ToXml()
        {
            return new XElement("Item",
                new XElement("ContainerId", Container.Id.ToString()),
                new XElement("DifferenceName", DifferenceName.ToString()),
                new XElement("DifferenceCount", DifferenceCount.ToString()),
                new XElement("Comments", Comments));
        }
    }
}
