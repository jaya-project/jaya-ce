﻿using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    public class Provider
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string ProviderName { get; set; }

        public Provider()
        {}

        public XElement ToXml()
        {
            return new XElement("Provider",
                new XElement("ProviderId", Id.ToString()),
                new XElement("ProviderCode", Code),
                new XElement("ProviderName", ProviderName));
        }
    }
}
