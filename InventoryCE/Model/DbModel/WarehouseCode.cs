﻿using System;
using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    [Serializable]
    public class WarehouseCode
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public WarehouseCode() { }

        public WarehouseCode(long id, string code, string desc)
        {
            Id = id;
            Code = code;
            Description = desc;
        }

        public WarehouseCode(string code, string desc)
            : this(0, code, desc)
        { }

        public XElement ToXml()
        {
            return new XElement("WarehouseCode",
                new XElement("WarehouseCodeId", Id.ToString()),
                new XElement("WhCode", Code),
                new XElement("Description", Description));
        }
    }
}
