﻿using System;
using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    public class ForeignProvider
    {
        public long Id { get; set; }
        public long Code { get; set; }
        public String Name { get; set; }

        public ForeignProvider() { }

        public XElement ToXml()
        {
            return new XElement("ForeignProvider",
                new XElement("Id", Id.ToString()),
                new XElement("Code", Code.ToString()),
                new XElement("Name", Name));
        }
    }
}
