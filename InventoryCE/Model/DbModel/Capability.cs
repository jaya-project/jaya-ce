﻿namespace InventoryCE.Model.DbModel
{
    public class Capability
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Capability()
        {}

        public Capability(long id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }

        public Capability(string name, string description)
            : this(0, name, description)
        {}
    }
}
