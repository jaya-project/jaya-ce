﻿using System;
using System.Xml.Linq;

namespace InventoryCE.Model.DbModel
{
    [Serializable]
    public class Location
    {
        public long Id { get; set; }
        public string Position { get; set; }
        public string Row { get; set; }
        public string Column { get; set; }
        public string Depth { get; set; }
        public string Name { get; set; }
        public long WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }

        public Location(){}

        public XElement ToXml(params XElement[] containers)
        {
            XElement x = new XElement("Location",
                new XElement("LocationId", (Id > 0 ? Id.ToString() : "generate")),
                new XElement("Position", Position),
                new XElement("Row", Row),
                new XElement("Column", Column),
                new XElement("Depth", Depth),
                new XElement("LocationName", Name),
                new XElement("WarehouseId", Warehouse != null ? Warehouse.Id.ToString() : "1"));

            if (containers != null)
            {
                x.Add(new XElement("Containers", containers));
            }

            return x;
        }

        public string ToCsv()
        {
            return string.Format("Location: id={0},position={1},row={2},column={3},depth={4},name={5},warehouse_id={6}{7}",
                Id, Position, Row, Column, Depth, Name, WarehouseId, Environment.NewLine);
        }
    }
}
