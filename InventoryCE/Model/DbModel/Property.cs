﻿
namespace InventoryCE.Model.DbModel
{
    public class Property
    {
        public enum DataType
        {
            Object, String, Integer, Double, Float, Date
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DataType Type { get; set; }

        public Property() {}

        public Property(long id, string name, string value, DataType type)
        {
            Id = id;
            Name = name;
            Value = value;
            Type = type;
        }

        public Property(string name, string value, DataType type)
            : this(0, name, value, type)
        {
        }
    }
}