﻿using System.Collections.Generic;
using InventoryCE.Model.DbModel;

namespace InventoryCE.Model
{
    public class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Rut { get; set; }
        public string NickName { get; set; }
        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        public List<Capability> Capabilities { get; set; }

        public User()
        {}

        public User(string name, string lastName, string rut, string nickName, byte[] password, byte[] salt)
            : this(0, name, lastName, rut, nickName, password, salt)
        {}

        public User(long id, string name, string lastName, string rut, string nickName, byte[] password, byte[] salt)
        {
            Id = id;
            Name = name;
            LastName = lastName;
            Rut = rut;
            NickName = nickName;
            Password = password;
            Salt = salt;
        }

        private void ReadData()
        {
        }

        public bool Can(string action)
        {
            if (Capabilities != null && Capabilities.Count > 0)
            {
                Capability  cap = Capabilities.Find(c => c.Name == action);

                if (cap != null)
                    return true;
            }

            return false;
        }
    }
}
